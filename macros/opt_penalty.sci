function [fp, gradfp] = opt_penalty(f, gradf, g, gradg, alpha, penal)
// can also be called without gradients:
// fp = opt_penalty(f, g, alpha, penal)

  rhs = argn(2)
  gradients = %T
  if rhs == 4 then
    gradients = %F
    penal = gradg
    alpha = g
    g = gradf
  elseif rhs ~= 6 then
    error('bad number of arguments')
  end
  
  if gradients then
    [p, dp] = penal(g)
    gradfp = gradf + alpha * sum((dp' .*. ones(1, length(g))) .* gradg, 'r')
  else
    p = penal(g)
  end  
  fp = f + alpha * sum(p)
  
endfunction
