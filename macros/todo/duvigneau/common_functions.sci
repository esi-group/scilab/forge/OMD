
//=============================================================================
//If scale=1 then scale each coordinate so that it lies in [0,1]
//WARNING:
// We need to check that xmax ~= xmin
//=============================================================================
function [xs] = scale_coord(scale, x, xe)

rhs = argn(2)
[ndim,ndata] = size(x)

if scale==1 then
   xmin = min(x,'c');
   xmax = max(x,'c');
   rang = xmax - xmin
   for i=1:ndim //Check that xmin ne xmax
      if rang(i)==0.0 then
         rang(i) = 1.0
      end
   end
   if rhs==2 then
      for dim=1:ndim
         xs(dim,:) = x(dim,:) - xmin(dim);
         xs(dim,:) = xs(dim,:)/rang(dim);
      end
   else
      for dim=1:ndim
         xs(dim,:) = xe(dim,:) - xmin(dim);
         xs(dim,:) = xs(dim,:)/rang(dim);
      end
   end
   xs = xs - 0.5
else
   if rhs==2 then
      xs = x;
   else
      xs = xe;
   end
end


endfunction

//=============================================================================
//If scale=1 then scale each coordinate so that it lies in [0,1]
//WARNING:
// We need to check that xmax ~= xmin
//=============================================================================
function [xe] = unscale_coord(scale, x, xes)

rhs = argn(2)
if rhs~=3 then
   printf("unscale_coord: RHS must have three arguments\n")
   pause
end
[ndim,ndata] = size(x)

if scale==1 then
   xmin = min(x,'c');
   xmax = max(x,'c');
   rang = xmax - xmin
   for i=1:ndim //Check that xmin ne xmax
      if rang(i)==0.0 then
         rang(i) = 1.0
      end
   end
   for dim=1:ndim
      xe(dim,:) = xmin(dim) + rang(dim)*(xes(dim,:) + 0.5)
   end
else
   xe = xes
end


endfunction
//---------------------------------------------------------------------------
//Scale function values
//---------------------------------------------------------------------------
function fs = scale_func(scalef, f)

//Scale function values
if scalef==1 then
   minf = min(f)
   maxf = max(f)
   rang = maxf - minf
   if rang==0.0 then
      rang = 1.0
   end
   fs = (f - minf)/rang
else
   fs = f
end

endfunction

//----------------------------------------------------------------------------
//Un-Scale function values
//----------------------------------------------------------------------------
function fs = unscale_func(scalef, f, fs1)

//Scale function values
if scalef==1 then
   fmin = min(f)
   fmax = max(f)
   rang = fmax - fmin
   if rang==0.0 then
      rang = 1.0
   end
   fs = fmin + rang*fs1
else
   fs = fs1
end

endfunction

//----------------------------------------------------------------------------
//Unscale gradient
//----------------------------------------------------------------------------
function fd = unscale_grad(scalex, scalef, x, f, fsd)

[ndim,ndata] = size(x)

//Unscale coordinates
if scalex==1 then
   xmin = min(x,'c')
   xmax = max(x,'c')
   rang = xmax - xmin
   for i=1:ndim
      if rang(i)==0.0 then
         rang(i) = 1.0
      end
      fsd(i,:) = fsd(i,:) / rang(i)
   end
end

//Unscale function
if scalef==1 then
   fmin = min(f)
   fmax = max(f)
   rang = fmax - fmin
   if rang==0.0 then
      rang = 1.0
   end
   fsd = rang*fsd
end

fd = fsd

endfunction

//-----------------------------------------------------------------------------
//Unscale hessian
//-----------------------------------------------------------------------------
function fdd = unscale_hess(scalex, scalef, x, f, fsdd)

[ndim,ndata] = size(x)
[nr,nc]      = size(fsdd)
neval        = nc/ndim

//Unscale coordinates
if scalex==1 then
   xmin = min(x,'c')
   xmax = max(x,'c')
   rang = xmax - xmin
   for i=1:ndim
      if rang(i)==0.0 then
         rang(i) = 1.0
      end
   end
   smat = rang*rang'
   for i=1:neval
      n1 = (i-1)*ndim + 1
      n2 = n1 + ndim  - 1
      fsdd(:,n1:n2) = fsdd(:,n1:n2) ./ smat
   end
end

//Unscale function
if scalef==1 then
   fmin = min(f)
   fmax = max(f)
   rang = fmax - fmin
   if rang==0.0 then
      rang = 1.0
   end
   fsdd = rang*fsdd
end

fdd = fsdd

endfunction


//----------------------------------------------------------------------------
//PSO for finding best theta
//Currently only for scalar theta
//npart = Number of particles
//itmax = Max number of steps
//thmin = minimum value of theta
//thmax = maximum value of theta
//----------------------------------------------------------------------------
function [topt] = PSO(costfunc, x, f, npart, itmax, thmin, thmax, verbosity)

[lhs,rhs] = argn()
if rhs==7 then
   verbosity=1
end

//Sizes
[ndim,ndata] = size(x)
ntheta       = length(thmin)

if verbosity>=1 then
   if costfunc=='rbf' then
      printf("Running PSO for RBF\n");
   else
      printf("Running PSO for KRIGING\n");
   end
end

//Parameters for PSO
omg  = 1.2
phi1 = 2.0
phi2 = 2.0
h    = 3
alp  = 0.99
vf   = 0.98

//Convert to log
thmin = log10(thmin)
thmax = log10(thmax)

//Maximum velocity
vmax = 0.25*(thmax - thmin)

//Initialize theta
if ntheta==1 then
   theta = linspace(thmin,thmax,npart)
else
   theta  = []
   for i=1:ntheta
      theta1= thmin(i) + (thmax(i)-thmin(i))*grand(1,npart,'def')
      theta = [theta; theta1]
   end
end
theta = 10^(theta)

//Set initial cost function
if costfunc=='krig' then
   cost  = krig_cost(x, f, theta)
else
   cost  = rippa_cost(1, x, f, theta)
end

pcost = cost
ptheta= theta
[gcost,gind] = min(cost)
gtheta = theta(:,gind)

//Set zero initial velocity
v = zeros(ntheta,npart)

if verbosity>=1 then
//Visualize particle positions for 1-D cases
clf()
plot(0*ones(1,npart),log10(theta(1,:)),'o')
if ntheta==3 then
   plot(0*ones(1,npart),log10(theta(2,:)),'*')
   plot(0*ones(1,npart),log10(theta(3,:)),'+')
end
xtitle('', 'PSO Iteration', 'Parameter')
end

rtheta=[]

//Start PSO iterations
for it=1:itmax

   //Log of parameters
   l_theta = log10(theta)
   l_ptheta= log10(ptheta)
   l_gtheta= log10(gtheta)

   //Compute velocity
   r1      = grand(ntheta,npart,'def')
   r2      = grand(ntheta,npart,'def')
   vi      = omg*v
   vp      = phi1*r1.*(l_ptheta - l_theta)
   for i=1:ntheta
      vg(i,:)  = phi2*r2(i,:).*(l_gtheta(i) - l_theta(i,:))
   end
   //v       = vi + vp + vg
   //v       = omg*v + (1-omg)*(vp + vg)
   v       = 0.729*( v + vp + vg)

   //Check bounds on velocity
   for ip=1:npart
      for id=1:ntheta
         if abs(v(id,ip)) > vmax(id) then
            v(id,ip)     = sign(v(id,ip)) * vmax(id)
         end
      end
   end

   //Update parameters
   l_theta = l_theta + v
   th_old  = theta
   theta   = 10^(l_theta)

   //CHeck for bounds
   if costfunc=='krig' then
      for ip=1:npart
         //Update best position for each particle
         for id=1:ntheta
            if theta(id,ip) < 10^thmin(id) then
               theta(id,ip) = th_old(id,ip)
               v(id,ip)     = 0.0
            end
            if theta(id,ip) > 10^thmax(id) then
               theta(id,ip) = th_old(id,ip)
               v(id,ip)     = 0.0
            end
         end
      end
   end

   //Compute cost function
   if costfunc=='krig' then
      cost  = krig_cost(x,f,theta)
   else
      cost  = rippa_cost(1,x,f,theta)
   end

   for ip=1:npart
      //Update best position for each particle
      if cost(ip) < pcost(ip) then
         pcost(ip)    = cost(ip)
         ptheta(:,ip) = theta(:,ip)
      end
      //Update global best position
      if cost(ip) < gcost then
         gcost  = cost(ip)
         gtheta = theta(:,ip)
      end
   end

   //Modify inertia term
   gcost2(it) = gcost
   if it>h then
      if gcost==gcost2(it-h) then
         omg  = alp*omg
         //vmax = vf*vmax
      end
   end

   residue = PSO_Residue(thmin, thmax, l_theta)
   if residue<1.0e-2 then
      rtheta=[rtheta, gtheta]
   end

   //Plot the particles for visualization
   if verbosity >=1 then
   plot(it*ones(1,npart),log10(theta(1,:)),'o')
   if ntheta==3 then
      plot(it*ones(1,npart),log10(theta(2,:)),'*')
      plot(it*ones(1,npart),log10(theta(3,:)),'+')
   end

 
   end

end

//Final global optimal theta
topt = gtheta

if verbosity>=1 then
   if costfunc=='rbf' then
      printf("coefficient d''attenuation optimum"); disp(topt)
   elseif costfunc=='krig' then
     printf("theta optimum"); disp(topt)
   end
end

endfunction

//-----------------------------------------------------------------------------
//Residue for measuring PSO convergence
//-----------------------------------------------------------------------------
function residue = PSO_Residue(thmin, thmax, l_theta)

rangtheta   = thmax - thmin
l_theta_max = max(l_theta, 'c')
l_theta_min = min(l_theta, 'c')
res         = (l_theta_max - l_theta_min) ./ rangtheta
residue     = max(res)

endfunction


