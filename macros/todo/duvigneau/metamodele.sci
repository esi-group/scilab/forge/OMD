
//-----------------------------------------------------------------------------
//Construire un metamodele 
//-----------------------------------------------------------------------------

function [theta]=metaconst(method,basis,x,f,scalex,scalef,itmax)
[lhs,rhs]   = argn()
//Normaliser les variables de design et la fonction f
xs = scale_coord(scalex, x);
fs = scale_func(scalef, f);

//Modele RBF 
if method=='rbf' then
   afmin = 0.1;
   afmax = 5.0;
   npart = 5;
   //Optimiser le coefficient d'attenuation
   if basis==1 | basis==5 then
	af = PSO('rbf',xs,fs,npart,itmax,afmin,afmax)
   elseif basis==2 then
	printf("PSO is not defined for this case, basis = %d\n", basis)
	halt
   else
	af = 1
   end

   //Construire le modele RBF
   [w,acond]=rbf_net(basis, af, xs, fs);
   theta=[w;af]
   fprintfMat('rbf_w.dat', theta)
   
//Krigeage
elseif method=='krig' then 

   //Optimiser les parametres de la fonction de correlation
   npart = 10;
   thmin = [0.01*ones(ndim,1); 0.001; 0.001];
   thmax = [10*ones(ndim,1)  ; 1.000; 1.000];
   theta    = PSO('krig', xs, fs, npart, itmax, thmin, thmax)
   fprintfMat('krig_theta.dat', theta)

//Modele polynomial
else 
   [w,acond,err] = poly_net(xs, fs)
   fprintfMat('polyn_w.dat', w)
   theta=w
end

endfunction


//-----------------------------------------------------------------------------
//Evaluer la fonction
//-----------------------------------------------------------------------------
function [fr,frl,fru,var]=metaeval(method,basis,x,f,xe,scalex,scalef,theta)
[lhs,rhs]   = argn()
//Normaliser les variables de design et la fonction f
xs = scale_coord(scalex, x);
xes= scale_coord(scalex, x, xe);
fs = scale_func(scalef, f);

//Modele RBF 
if method=='rbf' then
   n=length(theta)
   w=theta(1:n-1)
   af=theta(n)
   fr = rbf_eval(basis, af, xs, fs, w, xes);
   if lhs>1 then
      var=0
      frl=fr
      fru=fr
   end

//Krigeage
elseif method=='krig' then 
   if lhs>2 then
      [fr,var] = krig_eval(xs, fs, theta, xes)
      frl = fr - sqrt(var);
      fru = fr + sqrt(var);
      frl = unscale_func(scalef, f, frl)
      fru = unscale_func(scalef, f, fru)
   else
      fr = krig_eval(xs, fs, theta, xes)
   end
  
//Modele polynomial
else 
   [fr] = poly_eval(xs, fs, theta, xes)
   if lhs>2 then
      var=0
      frl=fr
      fru=fr
   end
   
end

//Remettre la fonction a l'echelle
fr = unscale_func(scalef, f, fr)

endfunction
