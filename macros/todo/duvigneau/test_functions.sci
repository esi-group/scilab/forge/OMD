//-----------------------------------------------------------------------------
//Generate a uniform array of of nxn points in 2-D
//-----------------------------------------------------------------------------
function [x,x1,x2] = Uniform_Grid2(xmin,xmax,n)

dx   = (xmax - xmin)./(n - 1)
count= 1
for i=1:n(1)
   for j=1:n(2)
      x(1,count) = xmin(1) + dx(1)*(i-1)
      x(2,count) = xmin(2) + dx(2)*(j-1)
      count      = count + 1
   end
end

x1 = linspace(xmin(1), xmax(1), n(1))
x2 = linspace(xmin(2), xmax(2), n(2))

endfunction

//-----------------------------------------------------------------------------
//Generate a random array of nxn points in 2-D
//-----------------------------------------------------------------------------
function [x] = Random_Grid2(xmin,xmax,n,seed)

[lhs,rhs] = argn()
if rhs==3 then
   grand('setsd', seed)
end
ndim = length(xmin)
for i=1:n
   x(:,i) = xmin + (xmax-xmin).*grand(ndim,1,'def')
end

endfunction

//-----------------------------------------------------------------------------
//Gives the index of points to be used for training ANN
//Selects ntmax nearest points. If ntmax not specified, selects 2*ndim nearest 
//points
//-----------------------------------------------------------------------------
function [ind] = train_data_set(xp, x, ntmax)

rhs = argn(2)
[ndim,ndata] = size(x)

//If ntmax not given, use 2*ndim points for training
if rhs==2 then
   nt = 2*ndim
else
   nt = ntmax
end

//Remove points which are too close
stat = ones(1,ndata)
for i=1:ndata
   if stat(i)==1 then
      for j=i+1:ndata
         dr = norm(x(:,i)-x(:,j))
         if dr<%eps then
            stat(j) = 0
            printf("i=%d, j=%d, dr=%e\n", i, j, dr)
         end
      end
   end
end

//Find duplicate points and put a large value
sind      = find(stat==0)
n1        = ndata - length(sind)
x(:,sind) = 1.0e20

//Check there is sufficient data available
if nt > n1 then
   printf("train_data_set: ntmax is greater than ndata\n")
   printf("                available %d data points\n",n1)
   printf("                requested %d data points\n",nt)
   pause
end

//Find distance of xp to x and sort in increasing order
for i=1:ndata
   dr(i) = norm(xp - x(:,i))
end
[dr,ind] = gsort(dr, 'r', 'i')
ind      = ind(1:nt)

ind = ind' //Make ind a row vector

endfunction

//===========================================================================
//Test function
//===========================================================================
function [f,fd] = Test_Function(itest, x)

select itest
   //1-D test functions
   case 101 then
      m      = 2.0
      c      = 1.0
      [f,fd] = Test_Linear_1D(m, c, x)
   case 102 then
      a      = +2.0
      b      = -1.0
      c      = +5.0
      [f,fd] = Test_Quadratic_1D(a, b, c, x)
   case 103 then
      a      = 2*%pi
      b      = 0
      [f,fd] = Test_Sine_1D(a, b, x)
   case 104 then
      a      = 2*%pi
      b      = 0
      [f,fd] = Test_Sine2_1D(a, b, x)
   case 105 then
      [f,fd] = Test_Disc1_1D(x)
   //Higher-D test functions
   case 201 then
      [f,fd] = Test_Rastrigin(x)
   case 202  then
      [f,fd] = Test_Quadratic_2D(x) 
   else
      printf("Test_Function: Test function not defined\n")
      halt()
end

endfunction

//===========================================================================
//Linear function in 1D
//===========================================================================
function [f,fd] = Test_Linear_1D(m, c, x)

f  = m*x(1,:) + c
fd = m*ones(1,length(x))

endfunction

//===========================================================================
//Quadratic function in 1D
//===========================================================================
function [f,fd] = Test_Quadratic_1D(a, b, c, x)

f  = a*x.*x + b*x + c
fd = 2*a*x + b

endfunction

//===========================================================================
//Sine function in 1D
//===========================================================================
function [f,fd] = Test_Sine_1D(a, b, x)

f  = sin(a*x + b)
fd = a*cos(a*x + b)

endfunction

//===========================================================================
//Poly-Sine function in 1D
//===========================================================================
function [f,fd] = Test_Sine2_1D(a, b, x)

f  = x .* (1-x) .* sin(a*x + b)
fd = (1 - 2*x) .* sin(a*x + b) + a*x.*(1-x).*cos(a*x + b)

endfunction

//===========================================================================
//Quadratic function in 2D and more
//===========================================================================
function [f,fd] = Test_Quadratic_2D(x)
[ndim,ndata] = size(x)
f  =zeros(1:ndata)
fd=zeros(ndim,ndata)

for i=1:ndim
    f(1:ndata)=f(1:ndata)+x(i,1:ndata)**2
    fd(i,:)=2*x(i,:)
end


endfunction
//===========================================================================
//Rastrigin function
//===========================================================================
function [f,fd] = Test_Rastrigin(x)

[ndim,ndata] = size(x)
fd=zeros(ndim,ndata)
tmp = x.*x - 10.0*cos(2.0*%pi*x)
f   = 10*ndim + sum(tmp,'r')

for i=1:ndim
    fd(i,1:ndata)=2*x(i,1:ndata)+20*%pi*sin(2.0*%pi*x(i,1:ndata))
end

endfunction

