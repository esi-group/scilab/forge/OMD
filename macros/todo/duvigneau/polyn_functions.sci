//=============================================================================
//Construct a polynomial network
//=============================================================================
function [w,acond,err] = poly_net(x, f)

//Check number of input arguments
lhs = argn(1)
rhs = argn(2)
if rhs~=2 then
   printf("poly_net:\n")
   printf("Number of arguments should be 2\n")
   abort   
end

[ndim,ndata]=size(x);
[nrf,ncf]=size(f);

//Check for consistency of dimensions
if ncf~=ndata | nrf~=1 then
   printf("poly_net:\n")
   printf("Size of x and f are not same.\n")
   printf("f must be a row vector.\n")
   printf("Number of columns of x = number of columns of f.\n")
   abort   
end

//Construct coeffient matrix
A = [ones(ndata,1), x']
Z = []
for i=1:ndim
   x1= x(i,:)'
   for j=i:ndim
      x2= x(j,:)'
      x3= x1 .* x2
      Z = [Z, x3]
   end
end
A = [A, Z]

//Check the rank
rk  = rank(A)
rkf = (ndim+1)*(ndim+2)/2
if rk ~= rkf then
   printf("****************************************\n")
   printf("poly_net: Warning\n")
   printf("          Matrix does not have full rank\n")
   printf("          Rank      = %d\n", rk)
   printf("          Full rank = %d\n", rkf)
   printf("****************************************\n")
end

//Solve the linear system Aw=f
w = lsq(A, f')

//Find condition number of A
if lhs>=2 then
   acond = cond(A'*A)
end

//Residual error
if lhs==3 then
   res = A*w - f'
   err = norm(res)
end
endfunction

//=============================================================================
//Evaluate a polynomial network
//=============================================================================
function [fr,frd,frdd] = poly_eval(x, f, w, xe)

//Check number of input arguments
[lhs,rhs] = argn()
if rhs~=4 then
  printf("poly_eval: Number of arguments should be 4\n")
  abort
end

[ndim,ndata]=size(x);
[nrf,ncf]=size(xe);

//Check for consistency of dimensions
if nrf~=ndim then
  printf("poly_eval:\n")
  printf("Size of x and xe are not consistent.\n")
  printf("Number of row of x = number of rows of xe.\n")
  abort
end

//Construct coefficient matrix
for i=1:ncf
  A = [1, xe(:,i)'];
  Z = [];
  for k=1:ndim
    x1 = xe(k,i);
    x2 = xe(k:ndim,i)';
    x3 = x1 * x2;
    Z  = [Z, x3];
  end
  A = [A, Z]
  fr(i) = A*w
end

fr = fr'

//Hessian matrix from quadratic term
if lhs>=2 then
  count=(1+ndim)+1
  for id=1:ndim
    wm(id,id) = 2*w(count)
    count     = count + 1
    for jd=id+1:ndim
      wm(id,jd) = w(count)
      wm(jd,id) = w(count)
      count     = count + 1
    end
  end
end

//Compute gradient
if lhs>=2 then
  frd = []
  for i=1:ncf
    fd = w(2:2+ndim-1)
    fd = fd + wm*xe(:,i)
    frd= [frd, fd]
  end
end

//Compute Hessian
if lhs==3 then
  frdd = []
  for i=1:ncf
    fdd = wm
    frdd= [frdd, fdd]
  end
end
endfunction
