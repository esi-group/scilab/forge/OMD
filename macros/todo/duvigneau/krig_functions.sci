//-----------------------------------------------------------------------------
//Correlation between x1 and x2
//theta=1 or ndim
//-----------------------------------------------------------------------------
function [c] = correlation(x1, x2, theta)

ntheta = length(theta)
ndim   = length(x1)

if ntheta==ndim+2 then
   r    = theta(1:ndim)
   t1   = theta(ndim+1)
   t2   = theta(ndim+2)

   tmp  = (x1 - x2)./r
   tmp  = 0.5*sum( tmp .* tmp )
   c    = t1*exp(-tmp) + t2
elseif ntheta==3
   r    = theta(1)
   t1   = theta(2)
   t2   = theta(3)

   tmp  = (x1 - x2)
   tmp  = tmp .* tmp
   tmp  = 0.5*sum(tmp)/r/r
   c    = t1*exp(-tmp) + t2
else
   printf("correlation: Size of theta is not correct\n")
   printf("             is %d, should be 3 or ndim+2\n")
   halt
end

endfunction

//-----------------------------------------------------------------------------
//Correlation matrix
//-----------------------------------------------------------------------------
function [C] = corr_matrix(x, theta)

[ndim,ndata] = size(x)

C = zeros(ndata,ndata)
for i=1:ndata
   for j=1:ndata
      C(i,j) = correlation(x(:,i), x(:,j), theta)
   end
end

endfunction


//-----------------------------------------------------------------------------
//Computes logarithm of determinant using LU decomposition
//-----------------------------------------------------------------------------
function [ldet] = logdet(P, L, U, Q)

//PL   = full( diag(P*L) )
PL   = full( diag(L) )
PL   = abs(PL) //Avoid small negative values
//UQ   = full( diag(U*Q) )
UQ   = full( diag(U) )
UQ   = abs(UQ) //Avoid small negative values
ldet = sum( log(PL) ) + sum( log(UQ) )

endfunction

//-----------------------------------------------------------------------------
//Function to minimize in kriging
//-----------------------------------------------------------------------------
function [cost] = krig_cost(x, f, theta)

[nr,nc]     = size(theta)
[ndim,ndata]= size(x)

cost = []
for i=1:nc
   C         = corr_matrix(x, theta(:,i))
   condition = cond(C)
   if condition > 1/%eps then //Large condition number, cost function not 
      tmp = 1.0e20            //accurate. We ignore this point, give large cost
   else
      Csp       = sparse(C)
      [hand,rk] = lufact(Csp)
      tmp       = lusolve(hand, f')
      [P,L,U,Q] = luget(hand)
      ldetC     = logdet(P, L, U, Q)
      tmp       = f * tmp + ldetC
   end
   cost      = [cost, tmp]
end

endfunction


//-----------------------------------------------------------------------------
//Evaluate function values
//-----------------------------------------------------------------------------
function [fk,var] = krig_eval(x, f, theta, xe)

[lhs,rhs]   = argn()
[ndim,ndata]= size(x)
[nrf,ncf]   = size(f)
[nxer,nxec] = size(xe)
[nthr,nthc] = size(theta)
neval       = nxec

//Check consistency of dimensions
if ndata~=ncf then
   printf("x and f do not have consistent dimensions\n")
   halt()
end

if ndim~=nxer then
   printf("x and xe do not have same number of rows\n")
   halt()
end

if nthc~=1 then
   printf("theta must be a column vector\n")
   halt()
end

if nthr~=(ndim+2) & nthr~=3 then
   printf("Size of theta must be 3 or ndim+2\n")
   halt()
end

C         = corr_matrix(x, theta)
Csp       = sparse(C)
[hand,rk] = lufact(Csp)

fk        = []
var       = []
tmp1      = lusolve(hand,f')
for i=1:neval
   c    = []
   for j=1:ndata
      cc = correlation(xe(:,i), x(:,j), theta)
      c  = [c; cc]
   end
   ff = c' * tmp1
   fk = [fk, ff]
   if lhs==2 then       //Compute variance
      tmp2 = lusolve(hand, c)
      kap  = correlation(xe(:,i), xe(:,i), theta)
      vv   = kap - c' * tmp2
      var  = [var, vv]
   end
end

//Very small negative variance: If < %eps we zero it
ind      = find(abs(var) < 1.0e-10)
var(ind) = 0.0
ind      = find(var < 0.0)
if ind~=[] then
   printf("krig_eval WARNING: variance is negative\n")
end

endfunction


