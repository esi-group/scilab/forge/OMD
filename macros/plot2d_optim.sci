function plot2d_optim(i, y, bound, leq)
  rhs = argn(2)
  is_obj = rhs == 2
  if length(i) == 1 then
    x = i-1:i
  else
    x = i
  end
  n = length(x)
  drawlater;
  if is_obj then
    plot2d(x, y(x), style = 2);
  else
    col = 5 * ones(1, n)
    if leq then
      col(y(x) <= bound) = 3
    else
      col(y(x) >= bound) = 3
    end
    plot2d(x, y(x), style = col)
    plot2d(x, bound * ones(1, n))
  end
  drawnow;
endfunction
