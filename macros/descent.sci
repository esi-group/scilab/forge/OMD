// Steepest descent algorithm, see also %descent_*.sci
// G. Pujol, april 2008

function this = descent(param)
  this = mlist(['descent', 'iter', 'x0', 'eps', 'step', ...
		'stage', 'eval', 'x_k', 'y_k', 'dy_k', 'step_k'])

  // parameters
  this.iter = param.iter // maximum number of iterations
  this.x0 = param.x0  // initial point
  this.step = param.step // step size
  this.eps = getfield_deft('eps', param, 1E-10) // tolerance

  // internal variables
  this.stage = 0  // (stage=0: first iteration)
  this.eval = 0  // number of model evaluations
endfunction

