function disp_data(data, labels, colnames)

  rhs = argn(2)

  if type(data) == 1 then // (matrix of scalars)
    
    data = string(data)
    if rhs == 3 then
      colnames = string(matrix(colnames, 1, -1))
      if labels == '' then
	table = [colnames; data]
      else
	labels = string(matrix(labels, -1))
	table = ['', colnames; labels, data]
      end
    elseif rhs == 2 then
      labels = string(matrix(labels, -1))
      table = [labels, data]
    elseif rhs == 1 then
      table = data
    end
    table = justify(table, 'l')
    ncol = size(table, 2)
    f = emptystr(1, ncol + 1)
    f = strcat(f, '%s  ') + '\n'
    printf(f, table)
    
  elseif type(data) == 16 | type(data) == 17 then // (tlist or a mlist)

    allf = getfield(1, data)
    allf = allf(2 : $)
    if rhs == 1 then
      labels = allf
    end
    //ind = grep(allf, labels)
    ind = zeros(1, size(allf, 2)) ~= 0
    for l = labels
      ind = ind | (allf == l)
    end
    ind = find(ind)
    n = length(ind)
    if n > 0 then
      table = emptystr(n, 2)
      for i = 1 : n
        table(i,1) = allf(ind(i))
        v = sci2exp(getfield(ind(i) + 1, data), 0)
        if length(v) > 60 then
	  table(i,2) = '(line too long...)'
        else
	  table(i,2) = v
        end
      end
      table = justify(table, 'l')
      printf('%s %s\n', table)
    end
    
  end
  
endfunction
