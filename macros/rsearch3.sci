function this = rsearch3(param)
  this = mlist(['rsearch3', 'd', 'x_min', 'x_max', 'x0', 'nb_max_eval', ...
		'nb_eval', 'x_best', 'y_best', 'Cov', 'bias', 'dev', ...
		'step', 'iter'])
  this.d = param.d
  this.x_min = getfield_deft('x_min', param, zeros(1, param.d))
  this.x_max = getfield_deft('x_max', param, ones(1, param.d))
  x0 = (this.x_max - this.x_min) .* grand(1, this.d, 'def') + this.x_min
  this.x0 = getfield_deft('x0', param, x0)
  this.nb_max_eval = param.nb_max_eval
  this.nb_eval = 0
  this.x_best = this.x0
  this.y_best = %inf
  sigma = param.sigma
  this.Cov = diag(sigma.^2)
  this.bias = 0
  this.step = 0
  this.iter = 0
endfunction
