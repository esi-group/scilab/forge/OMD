function this = %mulambda_tell(this, x, y)
  y = matrix(y, 1, -1)

  // sigma adaptation ("1/5th rule") (a revoir)
  yopt = min(this.parents_y)
  if yopt ~= [] then
    p = sum(y < yopt) / this.lambda
    if p > 0.2 then
      this.sigma = this.sigma * this.a
    elseif p < 0.2 then
      this.sigma = this.sigma / this.a
    end
  end
  
  // selection of the mu best points (i.e. the parents of the next
  // generation)
  if this.keep_parents then
    x = [this.parents_x; x]
    y = [this.parents_y, y]
  end
  [s, k] = gsort(y, 'c', 'i')
  sel = k(1 : this.mu)
  this.parents_x = x(sel,:)
  this.parents_y = y(sel)
  this.xbar = mean(this.parents_x, 'r')
  
  this.iter = this.iter - 1
endfunction
