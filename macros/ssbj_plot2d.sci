function ssbj_plot2d(x)

  S = x(3)
  phi0w = x(4) * %pi / 180
  phi100w = x(5) * %pi / 180
  xlw = x(6)
  phi0t = x(8) * %pi / 180
  phi100t = x(9) * %pi / 180
  xlt = x(10)
  
  // *** Voilure ***
  
  // corde d'emplanture
  Cr = sqrt(S * (tan(phi0w) - tan(phi100w)) / (1 - xlw^2))
  // corde en bout d'aile
  Ctip = xlw * Cr
  // envergure
  b = 2 * Cr * (1 - xlw) / (tan(phi0w) - tan(phi100w))
  
  vertices = zeros(6, 2)
  vertices(1, :) = [0, 0]
  vertices(4, :) = vertices(1, :) + [Cr, 0]
  vertices(5, :) = vertices(4, :) + [b / 2 * tan(phi100w), b / 2]
  vertices(6, :) = vertices(5, :) + [-Ctip, 0]
  vertices(3, :) = vertices(4, :) + [b / 2 * tan(phi100w), -b / 2]
  vertices(2, :) = vertices(3, :) + [-Ctip, 0]
  i = [1 : 6, 1]
  subplot(121)
  plot2d(vertices(i,1), vertices(i,2))
  title("Wing")

  // *** D�rive ***

  // surface
  St = 0.1 * S
  // corde d'emplanture
  Crt = sqrt(St / 2 * (tan(phi0t) - tan(phi100t)) / (1 - xlt^2))
  // corde en bout d'aile
  Ctipt = xlt * Crt
  // envergure
  bt = Crt * (1 - xlt) / (tan(phi0t) - tan(phi100t))
  
  vertices = zeros(4, 2)
  vertices(1, :) = [0, 0]
  vertices(2, :) = vertices(1, :) + [Crt, 0]
  vertices(3, :) = vertices(2, :) + [bt * tan(phi100t), bt]
  vertices(4, :) = vertices(3, :) + [-Ctipt, 0]
  i = [1 : 4, 1]
  subplot(122)
  plot2d(vertices(i,1), vertices(i,2))
  title("Tail")
  
endfunction
