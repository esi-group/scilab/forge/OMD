function res = crossvalid(model, X, y, p, k, mix)

  // argument checking and default values
  rhs = argn(2)
  if rhs < 4 then
    error('bad number of arguments')
  end
  if typeof(model) == 'string' then
    model = evstr(model)
  end
  n = size(X, 1)
  if rhs < 6 then
    mix = %T
    if rhs < 5 then
      k = n
    end
  end
  
  // mix data before validation (if requested)
  if mix then
    i = grand(1,'prm',(1:n)')
    X = X(i,:)
    y = y(i,:)
  end
  
  fp = [(0 : (k - 1)) * floor(n / k) + 1, n + 1] // indices of (f)irst (p)oint
                                                 // of each bloc
  res = zeros(k, 6) // 6 quality criteria (see below)
  
  for i = 1 : k
  
    // indt: indices of the points of the test sample
    indt = fp(i) : (fp(i + 1) - 1)
    
    // indv: indices of the points of the validation sample
    // (i.e. complementary of indt in 1:n)
    bi = (zeros(1, n) == 0)
    bi(indt) = %F
    //indv = 1 : n
    //indv = indv(bi)
    indv = find(bi)
    
    // metamodel estimation on the validation sample
    f = model(X(indv,:), y(indv), p)

    // test and predicted responses
    yt = y(indt)    
    yp = f(X(indt,:))

    // quality criteria: R2, MSE, NMSE, SMSE, NB, MR
    res(i) = [1 - sum((yt - yp).^2) / sum((mean(yt) - yt).^2), ...
	      mean((yt - yp).^2), ...
	      mean((yt - yp).^2) / mean(yt) / mean(yp), ...
	      mean((yt - yp).^2) / stdev(yt) / stdev(yp), ...
	      (mean(yt) - mean(yp)) / mean(yt), ...
	      max(abs(yt - yp))]
  end
  
endfunction
