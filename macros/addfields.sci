function l2 = addfields(l1, typ, varargin)
  f1 = getfield(1,l1)
  //c1 = f1(1)
  f1 = f1(2:$)
  c2 = typ(1)
  f2 = typ(2:$)
  l2 = mlist([c2, f1, f2])
  for i = f1
    l2(i) = l1(i)
  end
  n2 = size(f2,2)
  if length(varargin) == n2 then
    for i = 1:n2
      l2(f2(i)) = varargin(i)
    end
  end
endfunction
