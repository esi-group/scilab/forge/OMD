function this = ssbj(param)
  rhs = argn(2)
  if rhs == 0 then param = struct(), end

  x_names = ['z', 'xmach', 'S', 'phi0w', 'phi100w', 'xlw', 't_cw', ...
	     'phi0t', 'phi100t', 'xlt', 't_ct', 'dfus', 'wfuel', ...
	     'alpha', 'xfac']
  x_min = getfield_deft('x_min', param, [8000, 1.6, 100, 40, ...
		    -10, 0.05, 0.04, 40, 0, 0.05, 0.05, 2, 15000, 10, 0.85])
  x_max = getfield_deft('x_max', param, [18500, 2, 200, 70, ...
		    20, 0.5, 0.08, 70, 10, 0.5, 0.08, 2.5, 40000, 15, 0.95])

  tow_max = getfield_deft('tow_max', param, 50000)
  ract_min = getfield_deft('ract_min', param, 7200000)
  dist_max = getfield_deft('dist_max', param, 1800)
  vref_max = getfield_deft('vref_max', param, 60)

  this = mlist(['ssbj', 'x_names', 'x_min', 'x_max', 'tow_max', ...
		'ract_min', 'dist_max', 'vref_max'], x_names, x_min, ...
	       x_max, tow_max, ract_min, dist_max, vref_max)
endfunction
