function this = %nmomd_tell(this, x, y)
  this.f_hist = [];
  this.x_hist = list();

  if this.init then
    [this.x_init, this.data_next, eval_func, f_hist, x_hist] = step_nelder_mead(y, x, [], 'init', this.log, this.kelley_restart, this.kelley_alpha);
    this.init = %F;
    this.f_hist = f_hist;
    this.x_hist = x_hist;
  else
    [this.x_init, this.data_next, eval_func, f_hist, x_hist] = step_nelder_mead(y, x, this.data_next, 'run', this.log, this.kelley_restart, this.kelley_alpha);
    this.f_hist = f_hist;
    this.x_hist = x_hist;
  end

  this.ItMX = this.ItMX - 1;
  this.stop = this.stop | (this.ItMX <= 0);
endfunction 
