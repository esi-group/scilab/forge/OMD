function this = %rsearch2_tell(this, x, y)
  this.nb_eval = this.nb_eval + 1
  if y < this.y_best then
    this.x_best = x
    this.y_best = y
  end
  // prepare next iteration
  dev = grand(1, 'mn', zeros(this.d, 1), this.Cov)'
  x_new = this.x_best + dev
  while ~ and(x_new >= this.x_min & x_new <= this.x_max)
    dev = grand(1, 'mn', zeros(this.d, 1), this.Cov)'
    x_new = this.x_best + dev
  end
  this.dev = dev
endfunction
