function out = randexp(param)
  warning('randexp deprecated, use rsearch instead');
  
// constructor of the randexp optimizer
// default parameters
   max_nb_anal = getfield_deft('max_nb_anal', param, 50);
   if ( hasfield('xmin',param) & hasfield('xmax',param) ) then
     if (length(param.xmin) ~=  length(param.xmax) ) then
       error('different number of boundaries in xmin and xmax'); 
     end
   else
     error('xmin and xmax need to be given to randexp (constructor)');
   end
 
  out = mlist(["randexp","max_nb_anal", "xmin", "xmax", "ii_int",...
        "x_hist","of_hist","g_hist","nb_anal"],max_nb_anal,...
        param.xmin,param.xmax,param.ii_int,[],[],[],0);
        
endfunction

