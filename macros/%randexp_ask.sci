function x = %randexp_ask(this)
  //// give one random point to evaluate
  n = length(this.xmin);
  x = this.xmin + (this.xmax-this.xmin).*grand(1,n,'def');
  // round integer variables
  x(this.ii_int)=round(x(this.ii_int));
  x = matrix(x, 1, -1); // make x as row
endfunction
