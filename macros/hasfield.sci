function out = hasfield(f, l)
  // like isfield, but works with struct, tlist and mlist types
  if typeof(l) == 'st'
    allf = getfield(1, l)
    out = or(allf(3:$) == f)
  elseif or(type(l) == [16, 17]) // tlist or mlist
    allf = getfield(1, l)
    out = or(allf(2:$) == f)
  else
    out = %F
  end
endfunction
