function this = lm(X, y, param)

  [n, d] = size(X)

  // defaut values for parameters
  rhs = argn(2)
  if rhs == 2 then
    param = struct()
  end
  bf = getfield_deft('basis_func', param, 'x(' + string(1 : d) + ')')
  weights = getfield_deft('weights', param, ones(1, n))

  // initialisation of the object
  this = mlist(['lm', 'X', 'y', 'weights', 'basis_func', 'coefficients', ...
		'residuals', 'fitted'])
  this.X = X
  this.y = matrix(y, -1)
  this.weights = weights
  this.basis_func = matrix(bf, 1, -1)
  
  // computation of regression coefficients, fitted values and residuals
  F = [ones(n, 1), apply_basis_func(this.X, this.basis_func)]
  W = diag(this.weights)
  this.coefficients = (F' * W * F) \ (F' * W * y)
  this.fitted = F * this.coefficients
  this.residuals = this.y - this.fitted

endfunction
