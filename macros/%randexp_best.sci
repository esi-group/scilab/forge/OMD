function [yopt,xopt] = %randexp_best(this) 
  // best according to penalized objective function
  nb_ofs = size(this.of_hist,'c');
  nb_constraints = size(this.g_hist,'c');
  // using default penalty parameters
  [iorder,is_feas,fp]=ord_lin_pena(nb_ofs,nb_constraints,this.of_hist,this.g_hist);
  // extract best, iorder(1)
  ib = iorder(1);
  xopt = this.x_hist(ib,:);
  yopt = mlist(['criteria', 'objective', 'constraint'],...
    this.of_hist(ib,:), this.g_hist(ib,:) );
endfunction
