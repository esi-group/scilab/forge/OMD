// ***********************************
// Here is a routine related to penalizing 
// objective functions, ordering them, etc.
//
// *** Often used symbols ***
// nd : number of designs to order
// mg : number of inequality constraints
// mf : number of objectives to minimize
// f_hist : matrix of objectives, nd*mf
// g_hist : matrix of inequality constraints, nd*mg
// pg : vector of mg constraints penalty parameters (>=0)
// pf : vector of mf objective penalty parameters (>=0)
//***************************
function [fp]=calc_lin_pen(nb_ofs,nb_constraints,f_hist,g_hist,pg, pf)
  warning('calc_lin_pen deprecated, use opt_penalty instead');

// Calculates
// fp(x) = \sum_i=1^mf pf(i)f_i(x) + \sum_i=1^mg pg(i)*max(0,g_i(x))
// for all nd points
  if (nb_ofs>0) then 
    [nd,mf]=size(f_hist);
  elseif (nb_constraints>0) then
    [nd,mg]=size(g_hist);
  else
    error("No objectives and no criteria");
  end


 fp = zeros(nd,1);
 for i=1:nd,
   fp(i)=0;
   if (nb_ofs>0) then
     fp(i)=(pf*(f_hist(i,:))');
   end
   if (nb_constraints>0) then
     fp(i) = fp(i)+(pg*(max(0,g_hist(i,:)))');
   end
 end
endfunction
