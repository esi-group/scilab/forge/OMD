function %lm_plot2d(this, which)

  rhs = argn(2)
  if rhs == 1 then
    which = 1
  end
  
  n = length(this.fitted)
  fitted = this.fitted
  residuals = this.residuals
  stdresiduals = wcenter(this.residuals)
  response = this.fitted + this.residuals
  
  select which
   
   case 1 then  // response vs fitted
    plot2d(fitted, response, style=-9)
    a = gca()
    a.x_label.text = 'Fitted values'
    a.y_label.text = 'Response values'
    title('Response vs Fitted')
    xlim = a.data_bounds
    xpolys(xlim(:,1), xlim(:,1))
    e = gce()
    e.children.line_style = 3
   
   case 2 then  // residuals vs fitted
    plot2d(fitted, residuals, style=-9)
    a = gca()
    a.x_label.text = 'Fitted values'
    a.y_label.text = 'Residuals'
    title('Residuals vs Fitted')
    xlim = a.data_bounds
    xpolys(xlim(:,1), [0; 0])
    e = gce()
    e.children.line_style = 3
    
   case 3 then  // QQ plot
    u = (1 : n) / (n + 1)
    quant_theo = cdfnor('X', zeros(1, n), ones(1, n), u, 1 - u)
    quant_emp = gsort(stdresiduals, 'g', 'i')
    plot2d(quant_theo, quant_emp, style=-9)
    a = gca()
    a.x_label.text = 'Theoretical Quantiles'
    a.y_label.text = 'Sample Quantiles'
    title('Normal Q-Q Plot')
    xlim = a.data_bounds
    xpolys(xlim(:,1), xlim(:,1))
    e = gce()
    e.children.line_style = 3
  
  end
  
endfunction
