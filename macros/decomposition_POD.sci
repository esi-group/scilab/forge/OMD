function [parametres,Base,P_moyen,critere_energetique] = decomposition_POD(P) 

// Ouput variables initialisation (not found in input variables)
parametres=[];
Base=[];
P_moyen=[];
critere_energetique=[];

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);


// INPUTS
// 
// P(m,n) : matrice de snapshots 
//          - m : nombre d''elements pour 1 snapshot
//          - n : nombre de snapshots
// 
// 
// OUTPUTS
// 
// parametres : coefficients de decomposition (pour chaque snapshot) dans la base (complete)
// 
// Base : ensemble des vecteurs propres de la matrice de covariance 
// 
// P_moyen : vecteur moyen (sur tous les snapshots)
// 
// critere_energetique : la i-eme composante de ce vecteur represente 
//                       la contribution energetique des i premiers modes
// 

nb_noeuds = size(mtlb_double(P),1);
nb_snapshots = size(mtlb_double(P),2);
size_L = min(nb_noeuds,nb_snapshots);

P_moyen = mean(P,2);
for i = 1:nb_noeuds
  P_(i,1:length(mtlb_double(P(i,:))-P_moyen(i))) = mtlb_double(P(i,:))-P_moyen(i);
end;

[Base,L] = svd(P_,"e");

nb_modes_conserves = 1;
critere = 0;
while nb_modes_conserves<=size_L
  critere = critere+(L(nb_modes_conserves,nb_modes_conserves)^2)/(norm(diag(L))^2);
  critere_energetique(1,nb_modes_conserves) = critere;
  nb_modes_conserves = nb_modes_conserves+1;
end;

// determination des parametres
parametres = P_'*Base;

return;
endfunction
