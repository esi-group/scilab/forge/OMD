function pairsplot(x, nclass)
  rhs = argn(2)
  if rhs == 1 then
    nclass = 10
  elseif rhs ~= 2 then
    error('Bad number of arguments')
  end
  
  [nr, nc] = size(x)
  if nc == 1 then
    error('x must have at least two columns')
  else
    for i = 1 : (nc-1)
      for j = (i+1) : nc
        // upper
	xsetech([(j - 1) / nc, (i - 1) / nc, 1 / nc, 1 / nc])
	plot2d(x(:, j), x(:, i), style = -9)
        // lower
	xsetech([(i - 1) / nc, (j - 1) / nc, 1 / nc, 1 / nc])
	plot2d(x(:, i), x(:, j), style = -9)
      end
      // diagonal
      xsetech([(i - 1) / nc, (i - 1) / nc, 1 / nc, 1 / nc])
      histplot(nclass, x(:, i))
    end
    // last diagonal element
    xsetech([(nc - 1) / nc, (nc - 1) / nc, 1 / nc, 1 / nc])
    histplot(nclass, x(:, nc))
  end
endfunction
