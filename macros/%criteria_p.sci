function %criteria_p(this)
  
  allf = getfield(1, this)
  allf = allf(2:$)
  // (cf help('mtlb_isfield')... )

  if or(allf == 'objective') then
    printf('objective:')
    disp(this.objective)
  end
  if or(allf =='list_grad_objective') then
    printf('list_grad_objective:')
    disp(this.list_grad_objective)
  end
  if or(allf =='grad_objective') then
    printf('grad_objective:')
    disp(this.grad_objective)  
  end

  if or(allf == 'constraint') then
    printf('constraint:')
    disp(this.constraint)
  end
  if or(allf =='list_grad_constraint') then
    printf('list_grad_constraint:')
    disp(this.list_grad_constraint)
  end
  if or(allf =='grad_constraint') then
    printf('grad_constraint:')
    disp(this.grad_constraint)  
  end

  if or(allf == 'econstraint') then
    printf('econstraint:')
    disp(this.econstraint)
  end
  if or(allf =='list_grad_econstraint') then
    printf('list_grad_econstraint:')
    disp(this.list_grad_econstraint)
  end
  if or(allf =='grad_econstraint') then
    printf('grad_econstraint:')
    disp(this.grad_econstraint)  
  end

endfunction
