function [yopt, xopt] = %nmomd_best(this) 
  [xopt, yopt] = step_nelder_mead(this.f_init, this.x_init, this.data_next, 'exit', this.log, this.kelley_restart, this.kelley_alpha);
endfunction
