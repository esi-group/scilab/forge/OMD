function this = mulambda(param)
  if param.mu >= param.lambda then
    error('mu must be lower than lambda')
  end

  this = mlist(['mulambda', 'mu', 'lambda', 'x0', , 'iter', 'a', ...
		'sigma', 'keep_parents', 'parents_x', 'parents_y', 'xbar'])
  this.mu = param.mu
  this.lambda = param.lambda
  this.x0 = param.x0
  this.iter = param.iter
  this.a = getfield_deft('a', param, 1.5)
  this.sigma = param.sigma
  this.keep_parents = getfield_deft('keep_parents', param, %f)
  this.parents_x = []
  this.parents_y = []
  this.xbar = this.x0
endfunction
