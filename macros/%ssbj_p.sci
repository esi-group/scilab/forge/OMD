function %ssbj_p(this)
  
  printf('*** Super-Sonic Business Jet simulator ***\n')
  
  printf('\n')
  printf('Optimization variables ranges\n')
  disp_data([matrix(this.x_min, -1), matrix(this.x_max, -1)], ...
	    this.x_names, ['min', 'max'])

  printf('\n')
  printf('Constraints limits\n')
  names = ['tow_max', 'ract_min', 'dist_max', 'vref_max']
  disp_data(this, names)

endfunction
