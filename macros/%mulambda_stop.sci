function out = %mulambda_stop(this)
  out = this.iter <= 0
endfunction
