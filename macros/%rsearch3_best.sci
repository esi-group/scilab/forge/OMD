function [y_best, x_best] = %rsearch3_best(this)
  y_best = this.y_best
  x_best = this.x_best
endfunction