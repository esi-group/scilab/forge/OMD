function out = %rsearch2_stop(this)
  out = this.nb_eval >= this.nb_max_eval
endfunction
