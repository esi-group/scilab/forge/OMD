function [u_app] = diffuse(x_data_input,u_data,x_set_input,n_deg,%interp,w_sing,epsilon,factor_dim,factor_r_max,ri_strategy,r_max_user)

// Ouput variables initialisation (not found in input variables)
u_app=[];

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

// default values for parameters
if %nargin<11 then
  r_max_user = 0.2;
end
if %nargin<10 then
  ri_strategy = 0;
end
if %nargin<9 then
  factor_r_max = 1;
end
if %nargin<8 then
  factor_dim = 1;
end
if %nargin<7 then
  epsilon = 0.0000000001;
end
if %nargin<6 then
  w_sing = 1;
end
if %nargin<5 then
  interp = 1;
end
if %nargin<4 then
  n_deg = 2;
end
if %nargin<3 then
  printf("Not enough input parameters \n");
  printf("Abort \n");
  return;
end

// dimensions
n_data   = size(x_data_input,1);
n_dim    = size(x_data_input,2);
n_domain = size(x_set_input,1);
n_u      = size(u_data,2);
u_app    = zeros(n_domain,1);

// tests
if size(x_data_input,1)~=size(u_data,1) then
  printf("Input and output parameters (for initial data) have different sizes \n");
  printf("Abort \n");
  return;
end
if size(x_data_input,2)~=size(x_set_input,2) then
  printf("Initial and unknown sets have different sizes \n");
  printf("Abort \n");
  return;
end

// scaling of the variables
for i_dim = 1:n_dim
  scaling_(1,i_dim) = matrix(max(x_data_input(:,i_dim)) - min(x_data_input(:,i_dim)),1,-1);
  x_data = x_data_input(:,i_dim)/scaling_(i_dim);
  x_set  = x_set_input(:,i_dim)/scaling_(i_dim);
end

// calculation of the "global" (i.e. maximum) radius of influence
// based on all points of x_set
r_max_glob = calc_r_max_glob(x_set,x_data,n_deg,factor_dim);

for i_dom = 1:n_domain
  x = x_set(i_dom,:);
  // -- distance calculation
  for j = 1:n_data
    d(1,j) = distance(x_data(j,:),x);
  end
  // -- number of terms for a general polynomial p (of degree n_deg)
  [n_polyn_terms,n_terms_by_deg] = numbers_polyn(n_dim,n_deg);
  // -- radius of the influence zone
  nb_pts_in_zone = n_polyn_terms+n_dim*factor_dim;
  r_max = radius(x_data,x,d,nb_pts_in_zone);
  // -- weights
  for i_w = 1:n_data
    if (ri_strategy==0) then
      ri(1,i_dom) = matrix(r_max_glob*factor_r_max,1,-1);  // ri = max value
    elseif (ri_strategy==1) then
      ri(1,i_dom) = matrix(r_max_user,1,-1);  // ri chosen by the user
    else
      ri(1,i_dom) = matrix(r_max*factor_r_max,1,-1);  // ri different for each point in x_set
    end
    w(1,i_w) = matrix(w_ref(d(i_w)/ri(i_dom)),1,-1);
    // 
    if (%interp==1) then // interpolation weights
     if (w_sing==1) then
       w(i_w) = w(i_w) ./ (1+epsilon-w(i_w));  // singular weights
     end
    elseif (%interp==2) then // least squares => all weights are equal
     w(i_w) = 1;
    end
  end
  if (%interp==1) & (w_sing==0) then // non-singular weights
   sum_w = sum(w,"m");
   w = w/sum_w;
  end
  // -- initialization
  A = zeros(real(n_polyn_terms),real(n_polyn_terms));
  h = max(d);
  // -- matrix and vector to compute beta coefficients
  for j = 1:n_data
    A = (A+(w(j)*(p((x_data(j,:)-x)/h,n_deg)))*(p((x_data(j,:)-x)/h,n_deg)'));
    B(:,j) = w(j)*p((x_data(j,:)-x)/h,n_deg);
  end;
  // -- D (dimensionalize)
  D = zeros(real(n_polyn_terms),real(n_polyn_terms));
  ind_D = 1;
  D(ind_D,ind_D) = 1;
  for i_deg = 1:n_deg
    for i_s = 1:n_terms_by_deg(i_deg)
      ind_D = ind_D+1;
      D(ind_D,ind_D) = h^i_deg;
    end
  end

  for iu = 1:n_u
    // beta coefficients
    beta_ = ((A^(-1))*B)*u_data(:,iu);
    // coefficients for the polynomial
    a = (Q(x,n_deg)'*D)*beta_;
    // diffuse approximation/interpolation
    u_app(i_dom,iu) = p(x,n_deg)'*a;
  end
end

if (%interp==3) then // "enriched" diffuse approximation
 u_app_data = diffuse(x_data_input,u_data,x_data_input,n_deg,0,w_sing,epsilon,factor_dim,factor_r_max,ri_strategy,r_max_user);
 residual = -u_app_data+u_data;
 // Q for enriched approximation
 for i = 1:n_data
   for j = 1:n_data
     d = distance(x_data(i,:),x_data(j,:));
     Q_e(i,j) = w_ref(d/ri(i_dom));
   end
 end
 Q_e_inv = Q_e^(-1);

 for i_dom = 1:n_domain
   x = x_set(i_dom,:);
   // q for enriched approximation
   for j = 1:n_data
     d = distance(x_data(j,:),x);
     w(j) = w_ref(d/ri(i_dom));
   end

   // beta for enriched approximation
   for iu = 1:n_u
     beta_e = Q_e_inv*residual(:,iu);
     u_app(i_dom,iu) = u_app(i_dom,iu)+w*beta_e;
   end
 end
end

return
endfunction

// radius of the influence zone

function [r_max] = radius(x_data,x,d,nb_pts_in_zone)
// Ouput variables initialisation (not found in input variables)
r_max=[];

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

n_data = size(x_data,1);
[d_sorted,index_d_sorted] = gsort(d,'g','i');
r_max = d(index_d_sorted(min(nb_pts_in_zone,n_data)));
endfunction

// calculation of the maximum radius of the influence zone

function [r_max_glob] = calc_r_max_glob(x_set,x_data,n_deg,factor_dim)
// Ouput variables initialisation (not found in input variables)
r_max_glob=[];

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

n_data   = size(x_data,1);
n_dim    = size(x_data,2);
n_domain = size(x_set,1);

for i_dom = 1:n_domain
  x = x_set(i_dom,:);
  // -- distance calculation
  for j = 1:n_data
    d(1,j) = distance(x_data(j,:),x);
  end
  // -- number of terms for a general polynomial p (of degree n_deg)
  [n_polyn_terms,n_terms_by_deg] = numbers_polyn(n_dim,n_deg);
  // -- radius of the influence zone
  nb_pts_in_zone = n_polyn_terms+n_dim*factor_dim;
  r_max(:,i_dom) = matrix(radius(x_data,x,d,nb_pts_in_zone),1,-1);
end
r_max_glob = max(r_max,"c");
endfunction

// window function

function [w_ref_] = w_ref(s)

// Ouput variables initialisation (not found in input variables)
w_ref_=[];

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

if (s<=1) & (s>=0) then
  w_ref_ = 1-3*(s^2)+2*(s^3);
else
  w_ref_ = 0;
end
endfunction

// Euclidean distance

function [distance_] = distance(a,b)
// Ouput variables initialisation (not found in input variables)
distance_=[];

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

distance_ = norm(a-b);
endfunction

// function calculating the number of terms in a polynomial

function [n_polyn_terms,n_terms_by_deg] = numbers_polyn(n_dim,n_deg) 

// Ouput variables initialisation (not found in input variables)
n_polyn_terms  = [];
n_terms_by_deg = [];

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

n_polyn_terms = 1;
for i_deg = 1:n_deg
  sum_terms = 0;
  for j = 1:i_deg
    if (n_dim==1) then
      sum_terms = n_dim;
    else
      sum_terms = sum_terms+nchoosek(n_dim,j);
    end
  end
  n_terms_by_deg(1,i_deg) = matrix(sum_terms,1,-1);
  n_polyn_terms = n_polyn_terms+sum_terms;
end
endfunction

// polynomial

function [p_] = p(x,n_deg)
// Ouput variables initialisation (not found in input variables)
p_=[];

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

n_dim = size(x,2);

p_(1,1) = 1;
for i_dim = 1:n_dim
  p_(1+i_dim,1) = x(i_dim);
end

if (n_deg==2) then
  for i_dim = 1:n_dim
    p_(1+n_dim+i_dim,1) = 0.5*(x(i_dim)^2);
  end
  ind_current = 1+n_dim*2;
  for i1 = 1:n_dim-1
    for i2 = i1+1:n_dim
      ind_current = ind_current+1;
      p_(ind_current,1) = 0.5*x(i1)*x(i2);
    end
  end
end
endfunction

// transformation matrix

function [Q_] = Q(x,n_deg)
// Ouput variables initialisation (not found in input variables)
Q_=[];

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

n_dim = size(x,2);

if (n_deg==1) then
  Q_ = zeros(n_dim+1,n_dim+1);

  Q_(1,1) = 1;
  for i = 1:n_dim
    Q_(i+1,1) = -x(i);
  end
  for i = 1:n_dim
    Q_(i+1,i+1) = 1;
  end
elseif (n_deg==2) then
  [n_polyn_terms,n_terms_by_deg] = numbers_polyn(n_dim,n_deg);
  Q_ = eye(real(n_polyn_terms),real(n_polyn_terms));
  for i = 1:n_dim
    Q_(i+1,1) = -x(i);
  end;
  for i = 1:n_dim
    Q_(i+n_dim+1,1) = 0.5*x(i)^2;
    Q_(i+n_dim+1,i+1) = -x(i);
  end;
  ind_current = 1+n_dim*2;
  for i1 = 1:n_dim-1
    for i2 = i1+1:n_dim
      ind_current = ind_current+1;
      Q_(ind_current,1) = 0.5*x(i1)*x(i2);
      Q_(ind_current,i1+1) = -0.5*x(i2);
      Q_(ind_current,i2+1) = -0.5*x(i1);
    end
  end
end
endfunction
