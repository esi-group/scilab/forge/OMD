function x = %rsearch1_ask(this)
  if this.nb_eval == 0 then
    x = this.x0
  else
    x = (this.x_max - this.x_min) .* grand(1, this.d, 'def') + this.x_min
  end
endfunction
