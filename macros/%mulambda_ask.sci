function x = %mulambda_ask(this)
  d = length(this.x0)
  x = zeros(this.lambda, d)
  for i = 1 : this.lambda
    x(i,:) = this.xbar + grand(1, d, 'nor', 0, this.sigma)
  end
endfunction
