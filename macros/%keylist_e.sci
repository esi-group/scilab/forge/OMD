function v = %keylist_e(fieldname, vdeft, this)
// The %nans or empty strings of v_init are replaced by vdeft, if
// vdeft is scalar, or by the corresponding value of vdeft, if
// vdeft has the same dimension as v_init. If v_def is not specified,
// then no repacement is done.
  
  rhs = argn(2)
  if rhs == 2 then
    this = vdeft
  elseif rhs ~= 3 then
    error('bad number of arguments')
  end

  // extract the requested field value
  allf = getfield(1, this)
  allf = allf(2 : $)
  i = find(allf == fieldname)
  if i ~= [] then
    v_init = getfield(i + 1, this)
  else
    error('field not found')
  end

  if rhs == 2 then
    // no default value -> just return the field value
    v = v_init
  else
    // default value -> replace missing elements
    
    v_init = matrix(v_init, 1, -1)
    vdeft = matrix(vdeft, 1, -1)
    v = v_init

    // check for missing values
    if (type(v) == 1) then  // (vector of scalars)
      empty = isnan(v)
    elseif (type(v) == 10) then  // (vector of strings)
      empty = (v == '')
    end
  
    // replace them
    if size(vdeft, 'c') == 1 then  // (default value is a scalar)
      v(empty) = vdeft
    elseif size(vdeft, 'c') == size(v_init, 'c') then
      v(empty) = vdeft(empty)
    else
      error('incompatible dimensions')
    end
  end
  
endfunction
