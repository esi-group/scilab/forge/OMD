function q = quantile(x, probs)
  x = matrix(x, 1, -1)
  k = gsort(x, 'g', 'i')
  p = linspace(0, 1, length(x))
  q = interpln([p; k], probs)
endfunction
