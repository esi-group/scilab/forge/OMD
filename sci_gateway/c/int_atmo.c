#include "stack-c.h"

extern int C2F(atmo)(double*, double*, double*, double*, double*);

int int_atmo(char* fname) 
{
  int m1, n1, l1;
  int l2, l3, l4, l5;

  CheckLhs(4, 4);
  CheckRhs(1, 1);

  GetRhsVar(1, "d", &m1, &n1, &l1);
  if (m1 != 1 || n1 != 1) {cerro("Argument 1: wrong type, expecting a scalar"); return 0;}

  CreateVar(2, "d", &m1, &n1, &l2);
  CreateVar(3, "d", &m1, &n1, &l3);
  CreateVar(4, "d", &m1, &n1, &l4);
  CreateVar(5, "d", &m1, &n1, &l5);

  C2F(atmo)(stk(l1), stk(l2), stk(l3), stk(l4), stk(l5));

  LhsVar(1) = 2;
  LhsVar(2) = 3;
  LhsVar(3) = 4;
  LhsVar(4) = 5;

  return 0;
}
