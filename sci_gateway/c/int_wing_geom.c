#include "stack-c.h"

extern int C2F(wing_geom)(double*, double*, double*, double*, double*, double*, 
			   double*, double*, double*, double*, double*, double*);

int int_wing_geom(char* fname) 
{
  int m1, n1, l1;
  int m2, n2, l2;
  int m3, n3, l3;
  int m4, n4, l4;
  int m5, n5, l5;
  int l6, l7, l8, l9, l10, l11, l12;

  CheckLhs(7, 7);
  CheckRhs(5, 5);

  GetRhsVar(1, "d", &m1, &n1, &l1);
  if (m1 != 1 || n1 != 1) {cerro("Argument 1: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(2, "d", &m2, &n2, &l2);
  if (m2 != 1 || n2 != 1) {cerro("Argument 2: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(3, "d", &m3, &n3, &l3);
  if (m3 != 1 || n3 != 1) {cerro("Argument 3: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(4, "d", &m4, &n4, &l4);
  if (m4 != 1 || n4 != 1) {cerro("Argument 4: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(5, "d", &m5, &n5, &l5);
  if (m5 != 1 || n5 != 1) {cerro("Argument 5: wrong type, expecting a scalar"); return 0;}

  CreateVar(6, "d", &m1, &n1, &l6);
  CreateVar(7, "d", &m1, &n1, &l7);
  CreateVar(8, "d", &m1, &n1, &l8);
  CreateVar(9, "d", &m1, &n1, &l9);
  CreateVar(10, "d", &m1, &n1, &l10);
  CreateVar(11, "d", &m1, &n1, &l11);
  CreateVar(12, "d", &m1, &n1, &l12);

  C2F(wing_geom)(stk(l1), stk(l2), stk(l3), stk(l4), stk(l5), stk(l6), 
		  stk(l7), stk(l8), stk(l9), stk(l10), stk(l11), stk(l12));

  LhsVar(1) = 6;
  LhsVar(2) = 7;
  LhsVar(3) = 8;
  LhsVar(4) = 9;
  LhsVar(5) = 10;
  LhsVar(6) = 11;
  LhsVar(7) = 12;

  return 0;
}
