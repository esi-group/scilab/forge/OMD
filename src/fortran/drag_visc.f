	subroutine drag_visc(z,xmach,xl,surf,Scdv)
	implicit real*8 (a-h,o-z)
c 
	call atmo(z,p,rho,T,sig)
c
	Ta = T*(1.d0+0.178d0*xmach**2)
c
	R1 = 1.d0+0.1151d0*xmach**2
	R2 = 0.5d0*(1.d0+R1)*R1**1.5
c
	xmu = 17.15d-6*(Ta/273.d0)**1.5*(273.d0+110.4d0)/
     &                                  (Ta+110.4d0)
c
	c = sqrt(1.4d0*p/rho)
c
	v = xmach*c
c
	Re = rho*v*xl/xmu
c
	cfi = 0.074d0/Re**0.2
c
	cf = R2**0.2/R1*cfi
c
	Scdv = cf*Surf
c
	return
	end


