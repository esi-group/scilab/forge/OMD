	subroutine aero(z,xmach,weight,s,phi0w,phi100w,t_cw,
     &                  aw,bw,crw,cmaw,sextw,
     &                  phi0t,phi100t,t_ct,bt,crt,cmat,sextt,
     &                  dfus,xlfus,sfus,deng,xlnac,igear,
     &                  Cl,Cd,drag)
c
	implicit real*8 (a-h,o-z)
c
	common /coeff_a/Cdw,Cdv,Cdi,pdyn,cx0,opol
c
	Neng = 2
c
	pi = 4.d0*atan(1.d0)
c
	call atmo(z,p,rho,T,sig)
c
	c = sqrt(1.4d0*p/rho)
	v = xmach*c
c
	Dnac   = 1.1d0*deng
c
	if(xmach.ge.1.d0) then
	  Amax_f = pi*dfus/4.d0
	  Amax_eng = pi*Dnac**2/4.d0 - 0.54d0*Deng**2
	  amax_eng = amax_eng/3.
	  call wing_area(dfus,bw,crw,phi0w,phi100w,t_cw,amax_w)
	  call tail_area(bt,crt,phi0t,phi100t,t_ct,amax_t)
	  Amax = 0.65*(Amax_f 
     &         + 2*Amax_w 
     &         + 1.2*Amax_t 
     &         + Neng*Amax_eng*0.38)
	  Ewd = 2.10d0
	  xmcdm = 1.d0/cos(phi0w*pi/180.d0)**0.2
	  if(xmach.ge.xmcdm)  then
	    Cdw = 4.5d0*pi/S*(Amax/xlfus)**2*ewd*
     &            (0.74d0+0.37d0*cos(phi0w*pi/180.d0))*
     &            (1.d0 - 0.3d0*sqrt(xmach-xmcdm))
	  else
	    Cdw = 4.5d0*pi/S*(Amax/xlfus)**2*ewd*
     &            (0.74d0+0.37d0*cos(phi0w*pi/180.d0))
	  endif
	else
	  Cdw = 0.d0
	endif
c
	pdyn = 0.5d0*rho*v**2
c
	Cl = 9.81d0*weight/(0.5d0*rho*v**2*S)
c
	call coef_pol(aw,phi0w,xmach,xk1)
c
	Cdi = xk1*Cl**2
c
	swetw = 2.d0*(1.d0+0.2d0*t_cw)*Sextw
	swett = 2.d0*(1.d0+0.2d0*t_ct)*Sextt
	swetn = pi*Dnac*xlnac
c
	call drag_visc(z,xmach,cmaw,swetw,Scdvw)
	call drag_visc(z,xmach,cmat,swett,Scdvt)
	call drag_visc(z,xmach,xlnac,swetn,Scdvn)
	call drag_visc(z,xmach,xlfus,sfus,Scdvf)
c
	Scdvisc = Scdvw + Scdvt + Scdvn*neng + Scdvf
	Cdv = Scdvisc / S
c
	if(igear.eq.1) then
	  Cdgear = 0.02d0
	else
	  Cdgear = 0.d0
	endif
c
	Cd = Cdw + Cdv + Cdgear + Cdi
	cx0 = cdw + cdv
	opol = xk1
c
	Drag = 0.5d0*rho*v**2*S*cd
c
	return
	end

