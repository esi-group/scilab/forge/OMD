	subroutine atmo(z,p,rho,T,sig)
	implicit real*8 (a-h,o-z)
c
	T0 = 288.15d0
	P0 = 101325.d0
	Th = -0.0065d0
	R  = 287.04d0
	g  = 9.80665d0
c
	rho0 = P0/R/T0
c
	if(z.le.11000.d0) then
	  T = T0 + Th*z
	  P = P0*(T/T0)**(-g/Th/R)
	  rho = P/R/T
	  sig = rho/rho0
	endif
c
	if(z.ge.11000.d0 .and. z.le.25000.d0) then
	  T = 216.65d0
	  P1 = P0*(T/T0)**(-g/Th/R)
	  P  = p1*exp(-g/R/T*(z-11000.d0))
	  rho = P/R/T
	  sig = rho/rho0
	endif
c
	if(z.ge.25000.d0) then
	  print*,'Atmo z = ',z,' doit etre < 25 000 ! STOP'
	  stop
	endif
c
	return
	end
