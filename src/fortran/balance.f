	subroutine balance(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,TOW,F0)
c----------------------------------------------------------------------
        implicit real*8 (a-h,o-z)
c
	dimension x(10),y(10),xt(10),yt(10),y0(10),d(10)
c
	common /eval_c/z_c,xmach_c,S_c,phi0w_c,phi100w_c,
     &  xlw_c,t_cw_c,phi0t_c,phi100t_c,xlt_c,t_ct_c,
     &  dfus_c,wfuel_c,alpha_c
c
	common /prsol/iprsol,icvg
	common /coeff_b/Cdw,Cdv,Cdi,fin
c
	external balance_feval
c
	z_c       = z
	xmach_c   = xmach
	S_c       = S
	phi0w_c   = phi0w
	phi100w_c = phi100w
	xlw_c     = xlw
	t_cw_c    = t_cw
	phi0t_c   = phi0t
	phi100t_c = phi100t
	xlt_c     = xlt
	t_ct_c    = t_ct
	dfus_c    = dfus
	wfuel_c   = wfuel
	alpha_c   = alpha
c
	TOW = 1.5d0*Wfuel
	F0 = 100.
c
	x(1) = TOW
	x(2) = F0
c
	n = 2
	itmax = 500
c
	call dfsol(n,x,itmax,balance_feval,xt,yt,y0,y,d)
c
	if(icvg.eq.0) then
	 print*,' z = ', z
	 print*,' xmach = ', xmach
	 print*,' s = ', S
	 print*,' phi0w = ', phi0w
	 print*,' phi100w = ', phi100w
	 print*,' xlw = ', xlw
	 print*,' t_cw = ', t_cw
	 print*,' phi0t = ', phi0t
	 print*,' phi100t = ', phi100t
	 print*,' xlt = ', xlt
	 print*,' t_ct = ', t_ct
	 print*,' dfus = ', dfus
	 print*,' wfuel = ', wfuel
	 print*,' alpha = ', alpha
	 stop
	endif
c
	TOW = x(1)
	F0  = x(2)
c
	if(TOW.le.0. .or. F0.le.0.) then
	 print*,'TOW = ',TOW
	 print*,'F0  = ',f0
	 print*,' z = ', z
	 print*,' xmach = ', xmach
	 print*,' s = ', S
	 print*,' phi0w = ', phi0w
	 print*,' phi100w = ', phi100w
	 print*,' xlw = ', xlw
	 print*,' t_cw = ', t_cw
	 print*,' phi0t = ', phi0t
	 print*,' phi100t = ', phi100t
	 print*,' xlt = ', xlt
	 print*,' t_ct = ', t_ct
	 print*,' dfus = ', dfus
	 print*,' wfuel = ', wfuel
	 print*,' alpha = ', alpha
	  stop
	endif
c
	return
	end
c
	subroutine balance_feval(n,x,y)
        implicit real*8 (a-h,o-z)
	dimension x(*),y(*)
	common /eval_c/z,xmach,S,phi0w,phi100w,
     &  xlw,t_cw,phi0t,phi100t,xlt,t_ct,
     &  dfus,wfuel,alpha
c
	common /coeff_a/Cdw1,Cdv1,Cdi1
	common /coeff_b/Cdw,Cdv,Cdi,fin
c
	x(1) = abs(x(1))
	x(2) = abs(x(2))
c

	TOW = x(1)
	F0 = x(2)
c
c... Geometry
c
        call wing_geom(S,phi0w,phi100w,xlw,dfus,
     &   aw,bw,crw,cmaw,campw,phi25w,sextw)
c
        call tail_geom(S,phi0t,phi100t,xlt,
     &   at,bt,crt,cmat,phi25t,sextt)
        call fus_geom(dfus,xmach,wfuel,xlfus,Sfus)
	call eng_geom(F0,deng,xleng)
	call nac_geom(deng,xleng,dnac,xlnac)
c
	call prop_weight(F0,w_prop)
c
c... Weight
c
	call struc_weight(crw,bw,t_cw,xlw,phi25w,sextw,TOW,Wfuel,
     &   bt,t_ct,xlt,phi25t,sextt,dfus,xlfus,sfus,w_prop,z,w_struc)
c
	w_gear = 0.04d0*TOW
	W_NS   = 2500.d0
	TOW    = w_struc + w_gear + w_prop + w_NS + Wfuel
	w_ae   = 0.17d0*TOW
	TOW    = TOW + w_ae
c
	y(1)   = TOW - x(1)
c
c... Engine Thrust
c
	Wbc  = 0.95d0*TOW
c
	igear = 0
	call aero(z,xmach,wbc,S,phi0w,phi100w,t_cw,aw,bw,crw,
     &            cmaw,sextw,
     &            phi0t,phi100t,t_ct,bt,crt,cmat,sextt,
     &            dfus,xlfus,sfus,deng,xlnac,igear,cl,cd,drag)
c
	cdw = cdw1
	cdv = cdv1
	cdi = cdi1
	fin = cl/cd
c
	F = Drag
c
	call prop(z,xmach,F_F0,cs)
c
	neng = 2
	F0 = F/F_F0/neng
c
	call to_power(TOW,S,phi0w,phi100w,t_cw,aw,bw,crw,
     &            cmaw,sextw,alpha,
     &            phi0t,phi100t,t_ct,bt,crt,cmat,sextt,
     &            dfus,xlfus,sfus,deng,xlnac,F0,
     &            Fto)
c
	F0 = max(F0,Fto)
c
	y(2) = F0 - x(2)
c
	return
	end
	
