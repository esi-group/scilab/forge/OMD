	subroutine fus_geom(Df,xmach,Wfuel,xlfus,Sfus)
	implicit real*8 (a-h,o-z)
c
	pi = 4.d0*atan(1.d0)
c
	Vfuel = Wfuel / 790.d0
c
	xl1 = df/2.d0/tan(0.3*asin(1.d0/xmach))
	xl2 = 8.d0
	xl3 = Vfuel / pi / Df**2
	xl4 = Df/2.d0/tan(12.d0*pi/180.d0)
c
        xl1 = xl1 * 1.2
        xl2 = xl2 * 1.4
        xl3 = 5+ xl3 * 1.7
        xl4 = xl4 * 1.1
c
	xlfus = xl1 + xl2 + xl3 + xl4
c
	Sfus = pi*Df*(xl2 + xl3 + 0.5d0*(sqrt(df**2/4.d0+xl1**2) +
     &                                   sqrt(df**2/4.d0+xl4**2)))
c
	return
	end

