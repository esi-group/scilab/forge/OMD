	subroutine tail_weight(bt,t_c,xl,phi25,sext,TOW,w_tail)
	implicit real*8 (a-h,o-z)
c
	pi = 4.d0*atan(1.d0)
	xnu = 4.5d0
c
	w_tail = 17.91d0*sext 
     &         + 3.36d-4*xnu*bt**3*(8.d0+0.09012d0*TOW/sext)/
     &          (t_c*cos(phi25*pi/180.d0)**2*sext*(1.d0+xl))
c
	return
	end
