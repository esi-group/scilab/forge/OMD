	implicit real*8(a-h,o-z)
	common /prsol/iprsol
c
	iprsol = 1
c
	open(unit=1,file='dat')
	read(1,*)z
	read(1,*)xmach
	read(1,*)s
	read(1,*)phi0w
	read(1,*)phi100w
	read(1,*)xlw
	read(1,*)t_cw
	read(1,*)phi0t
	read(1,*)phi100t
	read(1,*)xlt
	read(1,*)t_ct
	read(1,*)dfus
	read(1,*)wfuel
	read(1,*)alpha
	read(1,*)xfact
	close(1)
c
	iprsol = 0
	call balance(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,TOW,F0)
c
	print*,'Balance TOW = ',TOW,' F0 = ',F0
c
	call breguetr(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,xfact,ract)
c
	print*,'breguet ract = ',ract,' m'
c
	call bfl(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,xfact,dist)
c	
	print*,'Bfl dist = ',dist,' m '
c
	call approach(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,xfact,vref)
c
	print*,'Approach vref = ',vref,' m/s'
c
	end
