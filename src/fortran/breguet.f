	subroutine breguet(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,xfact,ract)
c----------------------------------------------------------------------
        implicit real*8 (a-h,o-z)
c
	call balance(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,TOW,F0)
c
c... Geometry
c
        call wing_geom(S,phi0w,phi100w,xlw,dfus,
     &   aw,bw,crw,cmaw,campw,phi25w,sextw)
        call tail_geom(S,phi0t,phi100t,xlt,
     &   at,bt,crt,cmat,phi25t,sextt)
        call fus_geom(dfus,xmach,wfuel,xlfus,Sfus)
	call eng_geom(F0,deng,xleng)
	call nac_geom(deng,xleng,dnac,xlnac)
c
c... Engine Thrust
c
	Wbc  = 0.95d0*TOW
c
	igear = 0
	call aero(z,xmach,wbc,S,phi0w,phi100w,t_cw,aw,bw,crw,
     &            cmaw,sextw,
     &            phi0t,phi100t,t_ct,bt,crt,cmat,sextt,
     &            dfus,xlfus,sfus,deng,xlnac,igear,cl,cd,drag)
c
	call prop(z,xmach,F_F0,cs)
c
	neng = 2
c
	call atmo(z,p,rho,T,sig)
	c = sqrt(1.4*p/rho)
	v = xmach*c
c
	wec = TOW - 0.95d0*Wfuel
c
	ract = v/9.81d0/cs*Cl/Cd*log(wbc/wec)
c
	return
	end
c
