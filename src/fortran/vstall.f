	subroutine vstall(S,weight,alpha,phi0w,aw,vs)
	implicit real*8 (a-h,o-z)
c
	pi = 4.d0*atan(1.d0)
c
	z = 0
	call atmo(z,p,rho,T,sig)
c
	a = alpha*pi/180.d0
	f0 = phi0w*pi/180.d0
c
	cl=pi*aw/2.d0*sin(a)*cos(a)*
     &     (cos(a)+sin(a)*cos(a)/cos(f0)-sin(a)/2.d0/cos(f0))
c
	vs = sqrt(9.81d0*weight/(0.5d0*rho*S*cl))
c
	vs = 0.95*vs
c
	return
	end
	

