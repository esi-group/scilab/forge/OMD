	subroutine wing_geom(S,phi0,phi100,xl,df,aw,bw,croot,cma,
     &                       camp,phi25,sext)
     	implicit real*8 (a-h,o-z)
c
	pi = 4.d0*atan(1.d0)
c
	f0 = phi0*pi/180.d0
	f100 = phi100*pi/180.d0
c
	croot = sqrt(S*(tan(f0)-tan(f100))/(1.d0-xl**2))
	bw = 2.d0*croot*(1.d0-xl)/(tan(f0)-tan(f100))
	aw = bw**2/S
c
	phi25 = atan(0.75d0*tan(f0)+0.25d0*tan(f100))*180.d0/pi
c
	cma = 2.d0/3.d0*croot*(1.d0+xl+xl**2)/(1.d0+xl)
	camp = croot-df/2.d0*(tan(f0)-tan(f100))
c
	sext = s - (df*croot+df**2/4.d0*(tan(f100)-tan(f0)))
c
	return
	end

