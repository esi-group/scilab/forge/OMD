	subroutine wing_area(df,bw,cr,phi0,phi100,t_c,amax)
	implicit real*8 (a-h,o-z)
c
	pi = 4.d0*atan(1.d0)
c
	f0 = phi0*pi/180.d0
	f100 = phi100*pi/180.d0
c
	xmin=min(0.d0,bw/2.d0*tan(f0))
	xmax=max(cr,cr+bw/2*tan(f100))
c
	ymin = df/2.d0
	ymax = bw/2.d0
c
	amax = -1.d10
c
	nx = 50
	ny = 50
c
	dx = (xmax - xmin)/dble(nx-1)
	dy = (ymax - ymin)/dble(ny-1)
c
	do i=1,nx-1
	  x = xmin + (i-1)*dx
	  ax = 0.d0
	  do j=1,ny-1
	    y = ymin + (j-1)*dy
	    xle = y*tan(f0)
	    xte = cr + y*tan(f100)
	    if(xle.le.x .and. x.le.xte) then
	      c = xte - xle
	      x_c = (x-xle)/c
	      e =2.d0* 2.d0*t_c*x_c*(1.d0-x_c)*c
            else
	      e = 0.d0
            endif
	    ax = ax + e*dy
	  enddo
	  amax = max(amax,ax)
	enddo
c
	amax = 2.d0*amax
c
	return
	end
	
