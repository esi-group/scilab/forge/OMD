	subroutine tail_geom(S,phi0,phi100,xl,at,bt,croot,cma,
     &                       phi25,sext)
     	implicit real*8 (a-h,o-z)
c
	pi = 4.d0*atan(1.d0)
c
	f0 = phi0*pi/180.d0
	f100 = phi100*pi/180.d0
c
	Sv = 0.1d0*S
c
	croot = sqrt(Sv*(tan(f0)-tan(f100))/2.d0/(1.d0-xl**2))
	bt = croot*(1.d0-xl)/(tan(f0)-tan(f100))
	at = bt**2/Sv/2.d0
c
cprint*,'tail_geom '
cprint*,'s = ',s,' sv = ',sv
cprint*,'xl = ',xl
cprint*,'croot = ',croot,' bt = ',bt,' at = ',at
cprint*,' sqrt ',(Sv*(tan(f0)-tan(f100))/2.d0/(1.d0-xl**2))
c
cprint*,'f0 = ',f0,' f100 = ',f100
cprint*,'tan(f0)-tan(f100)=',tan(f0)-tan(f100)
cprint*,'phi0 = ',phi0,' phi100 = ',phi100
	phi25 = atan(0.75d0*tan(f0)+0.25d0*tan(f100))*180.d0/pi
cprint*,'phi25 = ',phi25
c
	cma = 2.d0/3.d0*croot*(1.d0+xl+xl**2)/(1.d0+xl)
c
	sext = Sv
c
	return
	end

