	subroutine breguetr(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,xfact,ract)
c----------------------------------------------------------------------
        implicit real*8 (a-h,o-z)
c
	dimension rp(100 000)
	common /coeff_a/cdw,cdv,cdi,pdyn,cx0,opol
c
	call zbqlini(0)
c
	call balance(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,TOW,F0)
c
c... Geometry
c
        call wing_geom(S,phi0w,phi100w,xlw,dfus,
     &   aw,bw,crw,cmaw,campw,phi25w,sextw)
        call tail_geom(S,phi0t,phi100t,xlt,
     &   at,bt,crt,cmat,phi25t,sextt)
        call fus_geom(dfus,xmach,wfuel,xlfus,Sfus)
	call eng_geom(F0,deng,xleng)
	call nac_geom(deng,xleng,dnac,xlnac)
c
c... Engine Thrust
c
	Wbc  = 0.95d0*TOW
c
	igear = 0
	call aero(z,xmach,wbc,S,phi0w,phi100w,t_cw,aw,bw,crw,
     &            cmaw,sextw,
     &            phi0t,phi100t,t_ct,bt,crt,cmat,sextt,
     &            dfus,xlfus,sfus,deng,xlnac,igear,cl,cd,drag)
c
	call prop(z,xmach,F_F0,cs)
c
	neng = 2
c
	call atmo(z,p,rho,T,sig)
	c = sqrt(1.4*p/rho)
	v = xmach*c
c
	pdy = 0.5*rho*v**2
	print*,'pdyn = ',pdyn,' pdy = ',pdy

c
	wec = TOW - 0.95d0*Wfuel
c
	ract0 = v/9.81d0/cs*Cl/Cd*log(wbc/wec)
c
	ns = 0  
	nf = 0
c
	zfw = tow - wfuel
c
	nsamp = 10 000
c
	do i=1,nsamp
	  cx0r = cx0*(1. + 2.d0*(-0.5 + rand())*0.04)
	  opolr = opol*(1. + 2.d0*(-0.5 + rand())*0.07)
	  csr = cs*(1. + 2.d0*(-0.5 + rand())*0.05)
	  zfwr = zfw*(1. + 2.d0*(-0.5 + rand())*0.08)
	  wbcr = 0.95*(zfwr + wfuel)
	  clr=9.81*wbcr/S/pdyn
	  cdr=cx0r + opolr*clr**2
	  wecr = wbcr/.95 - 0.95*wfuel
c
	  wbcr = 0.95*(zfwr + wfuel)
	  clr=9.81*wbcr/S/pdyn
	  cdr=cx0r + opolr*clr**2
	  wecr = wbcr/.95 - 0.95*wfuel
c
	  ract = v/9.81d0/csr*Clr/Cdr*log(wbcr/wecr)
	  rp(i)=ract/1882.
	  if(ract.ge.ract0) then
	    ns = ns + 1
	  else
	    nf = nf + 1
	  endif
	  write(79,*)ract
	  write(80,*)clr,cdr,csr,wbcr,wecr
	enddo
	print*,'ract0 = ',ract0
	print*,'proba ract >= ',ract0,' = ',float(ns)/float(nsamp)
	print*,'nsucces = ',ns,' fail = ',nf,' tot = ',nf+ns
	print*,'cl = ',cl
	print*,'cd = ',cd
	print*,'cs = ',cs
	print*,'wbc = ',wbc
	print*,'wec = ',wec
	print*,'cx0 = ',cx0
	print*,'opol = ',opol
	print*,'pdyn = ',pdyn
	print*,'s = ',s
c
        rmin = 1.d10
	rmax = -1.d10
	do i=1,nsamp
	  rmin = min(rmin,rp(i))
	  rmax = max(rmax,rp(i))
	enddo
	print*,'rmin = ',rmin
	print*,'rmax = ',rmax
c
	nb = 500
	dr = (rmax-rmin)/float(nb)
c
	do i=1,nb
	  r1 = rmin + (i-1)*dr
	  r2 = r1 + dr
	  np = 0
	  nh=0
	  do j=1,nsamp
	    if(rp(j).le.r2) np=np+1
	    if(r1.le.rp(j) .and. rp(j).lt.r2) nh=nh+1
	  enddo
	  write(81,*)r1,np,float(np)/float(nsamp)
	  write(82,*)0.5*(r1+r2),nh,float(nh)/float(nsamp)
	enddo
c
	ract = ract0
c
	return
	end
c
