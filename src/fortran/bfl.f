	subroutine bfl(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,xfact,dist)
c----------------------------------------------------------------------
        implicit real*8 (a-h,o-z)
c
	call balance(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,TOW,F0)

c
c... Geometry
c
        call wing_geom(S,phi0w,phi100w,xlw,dfus,
     &   aw,bw,crw,cmaw,campw,phi25w,sextw)
        call tail_geom(S,phi0t,phi100t,xlt,
     &   at,bt,crt,cmat,phi25t,sextt)
        call fus_geom(dfus,xmach,wfuel,xlfus,Sfus)
	call eng_geom(F0,deng,xleng)
	call nac_geom(deng,xleng,dnac,xlnac)
c
c... Engine Thrust
c
	igear = 1
	z0 = 0.d0
	call atmo(z0,p0,rho0,t0,sig)
	c0 = sqrt(1.4d0*P0/Rho0)
c
	call vstall(S,TOW,alpha,phi0w,aw,vs)
	vto  = 1.2*vs
	xmach0 = vto/c0
c
	call aero(z0,xmach0,TOW,S,phi0w,phi100w,t_cw,aw,bw,crw,
     &            cmaw,sextw,
     &            phi0t,phi100t,t_ct,bt,crt,cmat,sextt,
     &            dfus,xlfus,sfus,deng,xlnac,igear,cl,cd,drag)
c
	clm = 9.81d0*TOW/(0.5d0*rho0*S*vs**2)
	clcl = 9.81d0*TOW/(0.5d0*rho0*S*vto**2)
c
	neng = 2
c
	gc = asin(((neng-1)*F0-drag)/(9.81d0*TOW))
	if(neng.eq.2) then
	  gmin = 0.024d0
	else
	  gmin = 0.027d0
	endif
c
	g = gc - gmin
c
	if(g.lt.0) then
	  print*,'pb g = ',g
	endif
c
	u = 0.01d0*clm
	Tav = Neng*0.75d0*F0
c
	dist = 0.263d0/(1.d0+2.3d0*g)*
     &        (0.2044d0*TOW/(0.0829d0*S*rho0*cl)+35)*
     &        (1.d0/(Tav/(9.81d0*TOW)-U)+2.7d0) + 200.d0
c
	dist = 1.5*dist
c
	if(dist.le.0) then
	  print*,'g = ',g
	  print*,'tow = ',tow
	  print*,'S = ',s
	  print*,'rho0 = ',rho0
	  print*,'cl = ',cl
	  print*,'tav = ',tav
	  print*,'U = ',u
	  stop
	endif
c
	return
	end
c
