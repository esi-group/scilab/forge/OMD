<?xml version="1.0" encoding="ISO-8859-1"?>
<refentry version="5.0-subset Scilab" xml:id="OMD" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>26-Jan-2008</pubdate>
  </info>

  <refnamediv>
    <refname>OMD</refname>

    <refpurpose>OMD toolbox overview</refpurpose>
  </refnamediv>

  <refsection>
    <title>Architecture</title>

    <variablelist>
      <varlistentry>
        <term>criteria</term>

        <listitem>
          <para>criteria type as output to a simulator</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>optimizer</term>

        <listitem>
          <para>optimizer standardization</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>simulator</term>

        <listitem>
          <para>simulator standardization</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Ask and tell optimizers</title>

    <variablelist>
      <varlistentry>
        <term>descent</term>

        <listitem>
          <para>steepest descent optimizer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rsearch</term>

        <listitem>
          <para>random search optimizer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>mulambda</term>

        <listitem>
          <para>(mu/mu,+lambda)-ES optimizer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>cmaes</term>

        <listitem>
          <para>covariance matrix adaptation optimizer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>nm_ask_tell</term>

        <listitem>
          <para>Nelder-Mead (simplex) optimizer</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Optimization utilities</title>

    <variablelist>
      <varlistentry>
        <term>opt_penalty</term>

        <listitem>
          <para>penalized criterion for constrained optimization</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>pareto_front</term>

        <listitem>
          <para>Pareto-optimal points of a set of points</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>File interfacing</title>

    <variablelist>
      <varlistentry>
        <term>parse</term>

        <listitem>
          <para>read numbers and character strings from an ASCII file</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>template_replace</term>

        <listitem>
          <para>replaces shunks of code with values in a template file</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Metamodels</title>

    <variablelist>
      <varlistentry>
        <term>lm</term>

        <listitem>
          <para>fitting linear models</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>TO COMPLETE</term>

        <listitem>
          <para>with rbf, kriging, diffuse approx, POD, quad approx</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Statistics</title>

    <variablelist>
      <varlistentry>
        <term>crossvalid</term>

        <listitem>
          <para>metamodel cross-validation</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>lhsample</term>

        <listitem>
          <para>latin hypercube sampling (LHS)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>lm</term>

        <listitem>
          <para>fitting linear models</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>quantile</term>

        <listitem>
          <para>sample quantiles</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Graphics</title>

    <variablelist>
      <varlistentry>
        <term>pairsplot</term>

        <listitem>
          <para>scatterplot matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>pcoorplot</term>

        <listitem>
          <para>parallel coordinates plot</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>plot2d_optim</term>

        <listitem>
          <para>progress graph for optimization criteria</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Scilab language utilities</title>

    <variablelist>
      <varlistentry>
        <term>addfields</term>

        <listitem>
          <para>add fields to a mlist</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>disp_data</term>

        <listitem>
          <para>display formatted data</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>getfield_deft</term>

        <listitem>
          <para>list field extraction with default value handling</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>hasfield</term>

        <listitem>
          <para>checks if a field exists in an object</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <itemizedlist>
      <listitem>
        <para><emphasis>Description de l'architecture
        Scilab pour le projet OMD</emphasis>, G.Pujol and R. Le Riche,
        2008 (in French).</para>
      </listitem>

      <listitem>
        <para><emphasis>On object-oriented programming of optimizers --
        examples in Scilab</emphasis>, G. Pujol, Y. Collette, N. Hansen, D.
        Salazar and R. Le Riche, to appear in <emphasis>Multidisciplinary
        Design Optimization in Computational Mechanics</emphasis>, Wiley,
        2010.</para>
      </listitem>
    </itemizedlist>

    <para></para>
  </refsection>
</refentry>
