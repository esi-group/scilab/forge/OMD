// function to optimize
function y = f(x)
  y = x^2
endfunction

// setting parameters
x0      = 5.;
epsilon = 1.E-2;
alpha   = 1.E-1;
grad    = 1;

// initializations
xcur = x0;
ycur = f(xcur);

// optimization loop
while ~(abs(grad) < epsilon)
  // i++
  xlast = xcur;
  ylast = ycur;
  
  // point to evaluate
  xcur = xcur - alpha * grad;
  ycur = f(xcur);
  
  // updating the gradient
  grad = (ycur - ylast) / (xcur - xlast);
  
  // output
  printf('x = %f   y = %f\n', xcur, ycur); 
end
