function this = linreg(X, y, param)
  this = mlist(['linreg', 'weights', 'X', 'y', 'beta', 'sigma2']);
  this.weights = param.weights
  this.X = X
  this.y = y
  [n, d] = size(this.X)
  X = [ones(n, 1), this.X]
  y = matrix(this.y, -1)
  W = diag(this.weights)
  this.beta = (X' * W * X) \ (X' * W * y)
  this.sigma2 = sum((y - X * this.beta).^2) / (n - d - 1)
endfunction

function [y, v] = %linreg_e(X, this)
  n = size(X, 1)
  y = [ones(n, 1), X] * this.beta
  v = this.sigma2 * ones(n, 1)
endfunction
