// function to optimize
function y = f(x)
  y = x^2
endfunction

// setting parameters
x0      = 5.;
epsilon = 1.E-2;
alpha   = 1.E-1;
grad    = 1;

// initializations
xcur      = x0;
xlast     = xcur;
firstiter = %t;

// optimization loop
while ~(abs(grad) < epsilon)
  // point to evaluate
  if firstiter
      x = xcur;
  else
      x = xcur - alpha * grad;
  end
  y = f(x);
  
  // saving values
  xcur = x;
  ycur = y;
  
  // updating variables
  if firstiter
      firstiter = %f;
  else
      grad = (ycur - ylast) / (xcur - xlast);
  end
  
  // i++
  xlast = xcur;
  ylast = ycur;
  
  // output
  printf('x = %f   y = %f\n', xcur, ycur);
end
