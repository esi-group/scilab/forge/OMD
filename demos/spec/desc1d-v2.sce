// function to optimize
function y = f(x)
  y = x^2
endfunction

// setting parameters
x0      = 5.;
epsilon = 1.E-2;
alpha   = 1.E-1;
grad    = 1;

// initializations
xcur  = x0;
ycur  = f(xcur);
xlast = xcur;
ylast = ycur;

// optimization loop
while ~(abs(grad) < epsilon)
  // point to evaluate
  x = xcur - alpha * grad;
  y = f(x);
  
  // saving values
  xcur = x;
  ycur = y;
  
  // updating the gradient
  grad = (ycur - ylast) / (xcur - xlast);

  // i++
  xlast = xcur;
  ylast = ycur;
  
  // output
  printf('x = %f   y = %f\n', xcur, ycur);    
end
