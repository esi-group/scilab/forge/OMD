function y = mysim(x)
  y = mlist(['criteria', 'objective', 'grad_objective', ...
	     'econstraint', 'grad_econstraint'])
  xs = x(1)+x(2)+x(3)-1
  y.objective = (x(1)-x(3))^2 + 3*xs^2 + (x(1)-x(3)+1)^2
  y.grad_objective = [2*(x(1)-x(3)) + 6*xs + 2*(x(1)-x(3)+1), ...
		      6*xs, ...
		      -2*(x(1)-x(3)) + 6*xs - 2*(x(1)-x(3)+1)]
  y.econstraint = 1-x(1)-3*x(2)-x(3)^2
  y.grad_econstraint = [-1, -3, -2*x(3)]
endfunction

function [f, g, ind] = _costf(x, ind)
  global _lock
  global _y
  if ~_lock then
    f = _y.objective
    g = _y.grad_objective
    _lock = %T
  else
    f = 0
    g = zeros(x)
    ind = 0
  end
endfunction

function this = optimwrap(param)
  this = mlist(['optimwrap', 'nb_max_iter', 'iter', 'x', ...
		'xopt', 'fopt', 'gopt'])
  this.nb_max_iter = param.nb_max_iter
  this.iter = 0
  this.x = param.x0
endfunction

function x = %optimwrap_ask(this)
  x = this.x
endfunction

function this = %optimwrap_tell(this, x, y)
  this.iter = this.iter + 1
  this.xopt = x
  this.fopt = y.objective
  this.gopt = y.grad_objective 
  global _lock
  global _y
  _lock = %F
  _y = y
  [fopt, xopt] = optim(_costf, x', 'ar', 2)
  this.x = xopt'
endfunction

function out = %optimwrap_stop(this)
  out = this.iter >= this.nb_max_iter
endfunction

function [fopt, xopt] = %optimwrap_best(this)
  fopt = this.fopt
  xopt = this.xopt
endfunction

opt_param = struct();
opt_param.x0 = [0, 0, 0];
opt_param.nb_max_iter = 10;
opt = optimwrap(opt_param);

while ~stop(opt)
  x = ask(opt);
  y = mysim(x);
  opt = tell(opt, x, y);
  [fopt, xopt] = best(opt);
  printf('%i: fopt = %f, xopt = (%f, %f, %f)\n', opt.iter, fopt, xopt);
end
