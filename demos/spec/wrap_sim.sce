function y = mysim(x)
  y = mlist(['criteria', 'objective', 'grad_objective', ...
	     'econstraint', 'grad_econstraint'])
  xs = x(1)+x(2)+x(3)-1
  y.objective = (x(1)-x(3))^2 + 3*xs^2 + (x(1)-x(3)+1)^2
  y.grad_objective = [2*(x(1)-x(3)) + 6*xs + 2*(x(1)-x(3)+1), ...
		      6*xs, ...
		      -2*(x(1)-x(3)) + 6*xs - 2*(x(1)-x(3)+1)]
  y.econstraint = 1-x(1)-3*x(2)-x(3)^2
  y.grad_econstraint = [-1, -3, -2*x(3)]
endfunction

function [f, g, ind] = costf1(x, ind)
  y = mysim(x)
  f = y.objective
  g = y.grad_objective
endfunction

x0 = [0, 0, 0];
[fopt, xopt] = optim(costf1, x0)

function z = costf2(x)
  y = mysim(x(1:3))
  z = [y.grad_objective'+x(4)*y.grad_econstraint'; y.econstraint]
endfunction

x0 = [0, 0, 0, 0];
[x, v] = fsolve(x0, costf2)