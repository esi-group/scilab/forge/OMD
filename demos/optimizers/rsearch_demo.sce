// Demonstration of the functionalities of the rsearch optimizer
// Gilles Pujol, feb 2008

// example of Styblinski and Tang, 1990 (cf Spall, p. 46)
function y = f(x1, x2)
  y = 0.5 * ((x1^4 - 16 * x1^2 + 5 * x1) + (x2^4 - 16 * x2^2 + 5 * x2))
endfunction

param = struct();
param.d = 2;
param.x_min = [-8, -8];
param.x_max = [8, 8];

// Parameter user choices
items = ['blind'; 'localized'; 'enhanced'];
i = x_choose(items, ['Random search'; 'Choose algorithm']);
param.algorithm = items(i);
param.x0 = x_matrix('Starting point', [4, 6.4]);
param.nb_max_eval = evstr(x_dialog(['Maximum number of'; 'function evaluations'], '500'));
if i == 2 | i == 3 then
  param.sigma = x_matrix(['Standard deviations'; 'for local perturbations'], [3, 3]);
end

opt = rsearch(param);

// Contour plot
n = 100;
x1 = linspace(param.x_min(1), param.x_max(1), n);
x2 = linspace(param.x_min(2), param.x_max(2), n);
scf();
xset('fpf', ' ');
nl = 100;
contour2d(x1, x2, f, nl, style = color('gray')*ones(1, nl));

// Optimization
X = [];
Xopt = [];
i = 0;
while ~stop(opt)
  i = i + 1;
  x = ask(opt);
  y = f(x(1), x(2));
  opt = tell(opt, x, y);
  [yopt, xopt] = best(opt);
  printf('iter=%i, xopt=(%f,%f), yopt=%f\n', i, xopt, yopt);
  X = [X; x];
  if Xopt == [] | and(Xopt($,:) ~= xopt) then
    Xopt = [Xopt; xopt];
  end
end

plot2d(X(:,1), X(:,2), style = -1);
//e = gce();
//e.children.mark_foreground = color('blue');
plot2d(Xopt(:,1), Xopt(:,2), style = -1);
e = gce();
e.children.mark_foreground = color('blue');
xpoly(Xopt(:,1), Xopt(:,2))
e = gce();
e.foreground = color('blue');
e.thickness = 2;
