// ====================================================================
// This file is released into the public domain
// ====================================================================
demopath = get_absolute_file_path("omd_toolbox.dem.gateway.sce");

subdemolist = ["demo rsearch","optimizers/rsearch_demo.sce"];

subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================
