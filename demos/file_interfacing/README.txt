Examples of file interfacing (functions template_replace and parse)

* template : examples of use of the function template_replace (replacement of scilab code, comma handling).
* parse : examples of use of the function parse (empty field handling, type checking, comma handling).
* fletpow : a full example of an external simulator, here the Fletcher-Powell function coded in C++.
