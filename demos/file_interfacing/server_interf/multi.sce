global first_iter;
global server_path;
first_iter = %T;

function this = multi(param)
  rhs = argn(2)
  if rhs == 0 then
    param = struct()
  end
  this = mlist(['multi'])
endfunction

function y = %multi_e(x, this)
  global first_iter
  if first_iter then
    // lancement du serveur de calcul qui se met en attente	
    unix('xterm -sl 600 -sb -e ''cd ' + server_path + ';./launch_server'' &')
    first_iter = %F;
  end
  keys   = ['X', 'Y']
  values = [x(1), x(2)]
  template_replace('file_ros.tpl', keys, values, 'file.ros');
  unix('touch start.txt');

  // maintenant il faut attendre que le simulateur ait fini son boulot
  while %T 
    [x,test_file1] = fileinfo('output.txt');
    [x,test_file2] = fileinfo('end_ros.txt');
    if ( (test_file1==0) & (test_file2==0) ) then
      break
    end
  end
  //  Fin de la boucle d attente
  
  y        = mlist(['criteria', 'objective'], [0, 0])
  file_lat = mopen('output.txt');
  [res1]   = mfscanf(file_lat, '%g');
  mclose(file_lat);
  unix('rm output.txt');
  unix('rm end_ros.txt');
  y.objective(1) = res1;  
endfunction
