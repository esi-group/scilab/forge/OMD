Les fichiers sont les suivants :
- rosenbrock.c : le simulateur qui sera lance en serveur de calcul par launch_server. A compiler pour generer un executable avec : 
  gcc rosenbrock.c -o rosenbrock  
  rosenbrock affiche le resultat a l'ecran et l'ecrit dans un fichier output.txt
- launch_server : script en perl qui se charge d'intercepter le stdin et bloque tant que un fichier start.txt ou un fichier end.txt n'est pas cree. Si un fichier start.txt est cree, le fichier file.ros est execute, si un fichier end.txt est cree le fichier end.ros est execute (ce dernier fichier ne contient que la commande "END") et on quitte.
- file_ros.tpl: fichier "template" qui est utilises pour generer le fichier de donnees file.ros (voir l'utilisation de template_replace dans multi.sce)
- end.ros : fichier execute pour quitter le serveur de calcul
- multi.sce : fichier scilab qui sert de fonction cout. Il gere la boucle d'attente de retour des informations du calculateur (communication via le fichier output.txt)
- mulambda.sce : exemple d'optimisation avec l'algorithme mu-lambada en ask-tell
- test-server.sce : un simple test de fonctionnement des echanges entre le simulateur et Scilab avec gestion des boucles d'attente.

Attention, pour que tout cela fonctionne il faudra modifier dans :

test_server.sce, multi.sce, mulambda.sce les chemins d'acces a l'architecture Scilab et au launch_server (qui lui meme appelle reosenbrock)
