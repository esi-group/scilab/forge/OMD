#include <string>
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

typedef vector<double> VecDouble;

double fletcher_powell(const VecDouble &alpha, const VecDouble &A, const VecDouble &B, const VecDouble x) {
  int n = alpha.size();
  double y = 0.;
  double Ai, Bi;
  for (int i = 0; i < n; i++) {
    Ai = 0.;
    Bi = 0.;
    for (int j = 0; j < n; j++) {
      Ai = Ai + A[n*i+j] * sin(alpha[j]) + B[n*i+j] * cos(alpha[j]);
      Bi = Bi + A[n*i+j] * sin(x[j]) + B[n*i+j] * cos(x[j]);
    }
    y = y + pow(Ai - Bi, 2);
  }
  return y;
}


int main(int argn, char** argv) {

  string input_file_name(argv[1]);
  string output_file_name(argv[2]);
  
  ifstream input_file(input_file_name.c_str());

  string label = "";

  while (label != "n") {
    input_file >> label;
  }
  int n;
  input_file >> n;

  while (label != "alpha") {
    input_file >> label;
  }
  VecDouble alpha(n);
  for (int i = 0; i < n; i++) {
    input_file >> alpha[i];
  }

  while (label != "A") {
    input_file >> label;
  }
  VecDouble A(n * n);
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      input_file >> A[n*i+j];
    }
  }

  while (label != "B"){
    input_file >> label;
  }
  VecDouble B(n * n);
  for (int i = 0; i < n; i++){
    for (int j = 0; j < n; j++){
      input_file >> B[n*i+j];
    }
  }

  while (label != "x"){
    input_file >> label;
  }
  VecDouble x(n);
  for (int i = 0; i < n; i++){
    input_file >> x[i];
  }

  input_file.close();

  double y = fletcher_powell(alpha, A, B, x);

  ofstream output_file(output_file_name.c_str());
  output_file << "Execution de la fonction de fletcher powell sur " << endl << "x = ";
  for (int i = 0; i < n; i++) {
    output_file << x[i] <<"  ";
  }
  output_file << endl << "y = " << y << endl;
  output_file.close();

  return 0;
}
