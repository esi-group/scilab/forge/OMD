// A complete example of interface with an external simulator (fletpow.cpp)
// The communication is based on the template_replace and parse functions

// Generation of the input file from the template file

template_file_name = 'fletpow_input.txt.tpl';
input_file_name = 'fletpow_input.txt';
key_names = ['x1','x2','x3','x4'];
key_vals = [0.1, 0.2, 0.3, 0.4];
template_replace(template_file_name, key_names, key_vals, input_file_name);

// Call to the simulator

output_file_name = 'fletpow_output.txt';
//unix('fletpow.exe fletpow_input.txt fletpow_output.txt'); // windows
unix('./fletpow fletpow_input.txt fletpow_output.txt'); // linux
// (this writes fletpow_output.txt)

// Parse the output file for x and f(x)

outfile_name = 'fletpow_output.txt';
outkeys = ['x = ','y = '];
outkeys_type = ['real','real'];
outkeys_nbs = [4, 1];
[keyval, err] = parse(outfile_name, outkeys, outkeys_type, outkeys_nbs);
// keyval is a tlist of keylist type for which the () operator has
// been overloaded to ease programming
x = keyval(outkeys(1))
of = keyval(outkeys(2))
