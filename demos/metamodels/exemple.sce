//Lecture des parametres a partir d'un fichier
[fd,err]=mopen('./param_exple.inp', 'r');
[ndummy,dummy,method]  = mfscanf(fd, "%s %s");
[ndummy,dummy,ndim ]   = mfscanf(fd, "%s %d");
[ndummy,dummy,scalex ] = mfscanf(fd, "%s %d");
[ndummy,dummy,scalef ] = mfscanf(fd, "%s %d");
[ndummy,dummy,basis ]  = mfscanf(fd, "%s %d");
[ndummy,dummy,nom1]    = mfscanf(fd, "%s %s");
[ndummy,dummy,nom2]    = mfscanf(fd, "%s %s");
mclose(fd);

//Charger les differentes fonctions
exec('../macros/loader.sci',-1); 

//Lire une base de donnees
[fd] = file('open',nom1,'old');
L    = read(fd,-1,ndim+1);
file('close',fd);
x = L(:,1:ndim)';
f = L(:,ndim+1)';

//Lire les points test
[fd] = file('open',nom2,'old');
L    = read(fd,-1,ndim+1);
file('close',fd);
xe = L(:,1:ndim)';
fe = L(:,ndim+1)';
ntest = length(fe);

itmax = 100; //nbre d'iterations pour optimiser le metamodele

//construction du metamodele
theta=metaconst(method,basis,x,f,scalex,scalef,itmax);

//Evaluation sur les points test
[fr,frl,fru,var]=metaeval(method,basis,x,f,xe,scalex,scalef,theta);

//Estimation de l'erreur

//erreur locale
Df=fr-fe;
printf("Erreur max du metamodele %e\n", max(abs(Df)))

//erreur relative
Df1=zeros(fe)
for i=1:ntest
  if abs(fe(i))>1.e-14 then
    Df1(i)=100*Df(i)/fe(i)
  end
end
printf("Erreur relative max du metamodele %f %%\n", max(abs(Df1)))

//Enregistrer les resultats dans un fichier
result = [xe', fe', fr',Df',Df1'];
nom    = 'result_'+method+'_'+nom1
[fd]   = file('open',nom,'unkown')
if ndim==1 then
  for i=1:ntest
    write(fd,result(i,:),'(5(1x,e15.8))')
  end
elseif ndim==2 then
  n=sqrt(ntest)
  for i=1:n
    for j=1:n
      write(fd,result((i-1)*n+j,:),'(6(1x,e15.8))')
    end
    write(fd,' ')
  end
end

file('close',fd)
