lines(0);

//Lecture des parametres a partir d'un fichier
[fd,err]=mopen('./param_bdd.inp', 'r');
[ndummy,dummy,ndim ]   = mfscanf(fd, "%s %d");
[ndummy,dummy,itest ]  = mfscanf(fd, "%s %d");
[ndummy,dummy,xmin ]   = mfscanf(fd, "%s %f");
[ndummy,dummy,xmax ]   = mfscanf(fd, "%s %f");
[ndummy,dummy,ndata ]  = mfscanf(fd, "%s %d");
[ndummy,dummy,ntest ]  = mfscanf(fd, "%s %d");
[ndummy,dummy,dtype ]  = mfscanf(fd, "%s %s");
[ndummy,dummy,nom]     = mfscanf(fd, "%s %s");
mclose(fd);

//Charger les differentes fonctions
exec('../macros/loader.sci',-1); 

//Generer une base de donnees

//coordonnees des points de la base de donnees
if ndim==1 then     //fonction 1D
  if dtype=='uniform' then
    x        = linspace(xmin, xmax, ndata);
  else
    x        = xmin + (xmax-xmin)*grand(1,ndata,'def'); 
    x(1)     = xmin; 
    x(ndata) = xmax;
  end
  xe = linspace(xmin, xmax, ntest);
elseif ndim==2 then //fonction 2D
  xmin         = xmin*[1; 1];
  xmax         = xmax*[1; 1];
  [x,x1,x2]    = Uniform_Grid2(xmin, xmax, [ndata;ndata]);
  [xe,xe1,xe2] = Uniform_Grid2(xmin, xmax, [ntest;ntest]);
end

//Base de donnees
f = Test_Function(itest, x);

//Points test
fe = Test_Function(itest, xe);

//Ecriture des resultats dans des fichiers
Result = [x',f'];
[fd]   = file('open',nom,'unkown');
write(fd,Result,'(30(1x,e15.8))');
file('close',fd);

nom    = 'test_' + nom;
[fd]   = file('open',nom,'unkown');
Result = [xe',fe'];
write(fd,Result,'(30(1x,e15.8))');
file('close',fd);
