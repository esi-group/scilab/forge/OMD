set pm3d
set contour base
set cntrparam levels 20
unset clabel

set xlabel 'x'
set ylabel 'y'
set zlabel 'f(x,y)'

set style data lines
set xtics 0.2

set palette model HSV defined ( 0 0 1 1, 1 1 1 1 )

set view 72,30,1
set xrange[0:2]
set yrange[0:2]
#set zrange[0:10]

#set terminal postscript eps 18 enhanced color
#set size 1.25,1.4
#set output 'result_krig_rast_2d.eps'
set title 'fonction de Rastrigin par krigeage' ,-5 font "Times-Roman,30"



splot 'result_krig_bdd_rast_2d.dat' using 1:2:3 w l pal t 'fonction exacte', 'result_krig_bdd_rast_2d.dat' using 1:2:4 w lp t 'prediction'



pause -1

set zlabel 'Erreur'
#set zrange[-0.15:0.15]

#set terminal postscript eps 18 enhanced color
#set size 1.25,1.4
#set output 'erreur_krig_rast_2d.eps'
set title 'Erreur relative (%)' ,-5 font "Times-Roman,30"

splot 'result_krig_bdd_rast_2d.dat' using 1:2:6 w l pal notitle 

pause -1