function [f_] = f_1D(x)
  // Ouput variables initialisation (not found in input variables)
  f_=[];

  // Display mode
  mode(0);

  // Display warning for floating point exception
  ieee(1);

  f_ = 3+x.*cos(x./2)-sqrt(x)+3*sin((2*x).*sqrt(x))+exp(x*0.75);
endfunction
  
exec('./diffuse.sci');

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);

x_i = [];u_i = [];x = [];u_exact = [];

n_D = 1;

// valeurs donnees
//x_i(1:length([0,0.05,0.5,0.9,1,1.05,1.5,2.2,2.3,2.7,3,3.5,3.95,4,4.25,4.6,4.65,4.9,5]'),1) = [0,0.05,0.5,0.9,1,1.05,1.5,2.2,2.3,2.7,3,3.5,3.95,4,4.25,4.6,4.65,4.9,5]';
x_i = [0,0.05,0.5,0.9,1,1.05,1.5,2.2,2.3,2.7,3,3.5,3.95,4,4.25,4.6,4.65,4.9,5]';
u_i = f_1D(x_i);
 
// grille
//x(1:length((0:0.01:5)'),1) = (0:0.01:5)';
x       = [0:0.01:5]';
u_exact = f_1D(x);

n_deg        = 2;
_interp      = 1;
factor_dim   = 2;
factor_r_max = 1.1;

//function [u_app] = diffuse(x_data_input,u_data,x_set_input,n_deg,%interp,w_sing,epsilon,factor_dim,factor_r_max,ri_strategy,r_max_user)
  
// approximation diffuse
u_app = diffuse(x_i,u_i,x,n_deg,_interp,1,0.0000000001,factor_dim,factor_r_max,0);

// graphe
h = scf();
a = gca();
a.auto_clear = 'off';

plot(x_i,u_i,'ko');

plot(x,u_exact,"m");
plot(x,u_app,"r");

legend("exact","diffus");
xlabel("x");
ylabel("y");

a.grid = [1,1];
