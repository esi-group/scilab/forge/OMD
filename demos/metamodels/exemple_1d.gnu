set pm3d
set contour base
set cntrparam levels 20
unset clabel

set xlabel 'x'
set ylabel 'f(x)'

set style data lines
set xtics 0.2

set palette model HSV defined ( 0 0 1 1, 1 1 1 1 )

#set hidden3d

##############################
set view 72,30,1
set xrange[0:2]
set yrange[-1.5:1.5]

#set terminal postscript eps 18 enhanced color
#set size 1.25,1.4
#set output 'result_rbf_bdd_sin_1d.eps'
#set title 'Evaluation de f(x) par rbf' ,-5 font "Times-Roman,30"

plot 'result_rbf_bdd_sin_1d.dat' using 1:2 w l pal title 'fonction exacte','result_rbf_bdd_sin_1d.dat' using 1:3 w p pt 1 lt -1 title 'valeur predite'


pause -1

unset clabel
set yrange[-5:5]
set xlabel 'x'
set ylabel 'Erreur'
#set terminal postscript eps 18 enhanced color
#set size 1.25,1.4
#set output 'erreur_rbf_bdd_sin_1d.eps'
#set title 'Error relative sur f(x) (%)' ,-5 font "Times-Roman,30"

plot 'result_rbf_bdd_sin_1d.dat' using 1:5 w l pal notitle 

pause -1