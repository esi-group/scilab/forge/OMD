// Cnk, Pascal triangle
C   = cnk(2*n);
cn  = C(n+1,1:n+1);
c2n = C(2*n+1,1:2*n+1);

// construction of A (Hessian)
A=zeros(n+1,n+1);
for i=1:n+1
	for j=1:n+1
		A(i,j)=cn(i)*cn(j)/c2n(i+j-1);
	end
end
A=A/(2*n+1);

// constraints and nullspace projection matrix
Z=eye(n+1,n+1);
Z=Z(2:n,:)';
ZAZ=Z'*A*Z;

[U,D]=spec(ZAZ);
ZU=Z*U;

// eigenfunctions, Bézier curves
N=101;
t=linspace(0,1,N);
xs=linspace(0,1,n+1);
B=zeros(N,n-1);

for i=1:n-1
	B(:,i)=decas(t,ZU(:,i));
  Ufine(:,i) = degelevm1d(m+1,ZU(:,i));
end
DUf = 1./diag(sqrt(sum(Ufine.*Ufine,'row')));
Ufine = Ufine*DUf;
for i=1:n-1
	Bfine(:,i)=decas(t,Ufine(:,i));
end

