function [v] = degelev1d(u)
  n=length(u);
  m=n+1;
  v=zeros(m,1);

  v(1) = u(1);
  v(m) = u(n);
	
  for k=2:n
    a = (k-1)/n;
    v(k) = a*u(k-1) + (1-a)*u(k)
  end
endfunction

