function [v] = degelevm1d(m,u)
  n=length(u);
  v=zeros(m,1);
  v(1:n)=u;
  l=m-n;

  for i=0:l-1
    w=degelev1d(v(1:n+i));
    v=w(1:n+i+1);
  end
endfunction

