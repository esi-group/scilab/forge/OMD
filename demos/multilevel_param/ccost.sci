function [f,g,ind] = ccost(y,ind)
global A E Q x xtarget nbit
nbit = nbit + 1

f = recons(A,x+Q*E*y,xtarget);
g = grecons(A,x+Q*E*y,xtarget);
g = E'*Q*g;
endfunction

