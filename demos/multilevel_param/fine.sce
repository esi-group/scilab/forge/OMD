global x nbit

nbit = 0;
[Jf,xf,Gf] = optim(fcost, x,'qn','ar',nbf,100,1e-6);

x = xf;
G = Gf;
J = [J Jf];
