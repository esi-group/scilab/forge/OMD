function [f,g,ind] = fcost(x,ind)
global A xtarget nbit
nbit = nbit + 1

f = recons(A,x,xtarget);
g = grecons(A,x,xtarget);

g(1)=0d0;
g(length(g))=0d0;
endfunction

