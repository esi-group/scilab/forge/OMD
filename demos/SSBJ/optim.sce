sim = ssbj();
sim.ract_min = 6500000;
sim.dist_max = 1828;
sim.vref_max = 70;

x0 = [15557.936, 1.6857105, 116.41612, 48.207528, 14.933357, 0.1242700, ...
      0.0459469, 58.470062, 0.3001734, 0.2503178, 0.0701879, 2.2329986, ...
      19848.719, 14.794035, 0.9336053];
//x0 = [15029.8219, 1.80419439, 100., 52.0242245, 10.9333309, 0.0631019786, ...
//	0.0436489457, 68.0947767, 9.99479317, 0.443188622, 0.0503080707, ...
//	2.26402943, 15000., 14.9882846, 0.868905052]; // cf xopt.sce

n = 500;

opt_param = struct();
//opt_param.algorithm = 'blind'; // (commenter opt_param.x0 = x0)
//opt_param.algorithm = 'localized';
opt_param.algorithm = 'enhanced';
opt_param.d = length(sim.x_min);
opt_param.x_min = sim.x_min;
opt_param.x_max = sim.x_max;
opt_param.x0 = x0;
opt_param.nb_max_eval = n;
opt_param.sigma = (sim.x_max - sim.x_min) / 10;
opt = rsearch(opt_param);

// normalization constants
ko = sim.tow_max;
kc = [sim.ract_min, sim.dist_max, sim.vref_max];

alpha = 1;
deff('z=penal(y)', 'z=max(y,0)');

crit = zeros(n, 4);

scf();
subplot(221); xtitle('', 'nb iter', 'tow');
subplot(222); xtitle('', 'nb iter', 'ract');
subplot(223); xtitle('', 'nb iter', 'dist');
subplot(224); xtitle('', 'nb iter', 'vref');

i = 1;
while ~stop(opt)
  x = ask(opt);
  y = sim(x);
  fp = opt_penalty(y.objective(1)/ko, y.constraint(2:4)./kc, alpha, penal);
  opt = tell(opt, x, fp);
  [fopt, xopt] = best(opt);
  yopt = sim(xopt);
  crit(i,:) = [yopt.objective];
  crit(i,2) = - crit(i,2);
  if i > 1 then
    subplot(221); plot2d_optim(i, crit(:,1)');
    subplot(222); plot2d_optim(i, crit(:,2)', sim.ract_min, %F);
    subplot(223); plot2d_optim(i, crit(:,3)', sim.dist_max, %T);
    subplot(224); plot2d_optim(i, crit(:,4)', sim.vref_max, %T);
  end
  i = i + 1;
end

// best-of:
// [15094.747,1.6,100,40,-6.7768186,0.05,0.04,70,0,0.05,0.05,2,18479.68,15,0.85]
// [15098.148,1.6,100,40,-3.0406699,0.05,0.04,70,0,0.5,0.05,2,16948.777,15,0.95]
// [14883.797,1.6,100,46.904087,3.1267883,0.05,0.04,56.859914,10,0.5,0.05,2,15000,15,0.95]