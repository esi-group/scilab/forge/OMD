// Monte Carlo exploration of the SSBJ test case
// Gilles Pujol. Last modified on feb 23, 2008.

// parameters
n = 150; // number of simulations

// simulator initialization
sim = ssbj();
sim.ract_min = 6500000;
sim.dist_max = 1828;
sim.vref_max = 70;
disp(sim);
d = length(sim.x_min);

// design of experiments
x = lhsample(n, d);
x = x .* (ones(n,1) .*. (sim.x_max - sim.x_min)) + (ones(n,1) .*. sim.x_min);

// simulations
obj = zeros(n, 4);
for i = 1 : n
  y = sim(x(i,:));
  obj(i,1) = y('objective', 1);
  obj(i,2) = - y('objective', 2);
  obj(i,3) = y('objective', 3);
  obj(i,4) = y('objective', 4);
end

// parallel coordinates plot
data = [x, obj];
labels = [sim.x_names, 'tow', 'ract', 'dist', 'vref'];
sel = [min(data, 'r'); max(data, 'r')];
sel(2,16) = min(max(sim.tow_max, sel(1,16)), sel(2,16));
sel(1,17) = min(max(sim.ract_min, sel(1,17)), sel(2,17));
//sel(2,18) = min(max(sim.dist_max, sel(1,18)), sel(2,18));
//sel(2,19) = min(max(sim.vref_max, sel(1,19)), sel(2,19));
pcoorplot(data, labels, ones(1, n), %t, sel);
